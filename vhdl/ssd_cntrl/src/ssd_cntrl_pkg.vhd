-- TASK 2: put your code here

----------------------------------------------------------------------------------
--                                LIBRARIES                                     --
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.snake_game_pkg.all;
use work.n64_controller_pkg.all;

----------------------------------------------------------------------------------
--                                 PACKAGE                                      --
----------------------------------------------------------------------------------

package ssd_cntrl_pkg is
	--type GAME_STATE_T is (IDLE, PAUSED, RUNNING, GAME_OVER);
	--------------------------------------------------------------------
	--                          COMPONENT                             --
	--------------------------------------------------------------------
	component ssd_cntrl is
		-- generic( -- TASK 7: BONUS TASK
		-- 	NBR_CLK_CYCLES : integer
		-- );
		port (
			clk : in std_logic;
			res_n : in std_logic;
			--game_state : IN GAME_STATE_T;
			game_state : IN snake_game_state_t;
			n64_cntrl : IN N64_BUTTONS_T;

			hex0 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex1 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex2 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex3 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex4 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex5 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex6 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex7 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
		);
	end component;
end package;
