

library ieee;
use ieee.std_logic_1164.all;

-- import all required packages
use work.remote_bridge_pkg.all;

entity remote_bridge_top is
	port (
		--50 MHz clock input
		clk      : in  std_logic;

		-- push buttons and switches
		keys     : in std_logic_vector(3 downto 0);
		switches : in std_logic_vector(17 downto 0);
	
		--Seven segment displays
		hex0 : out std_logic_vector(6 downto 0);
		hex1 : out std_logic_vector(6 downto 0);
		hex2 : out std_logic_vector(6 downto 0);
		hex3 : out std_logic_vector(6 downto 0);
		hex4 : out std_logic_vector(6 downto 0);
		hex5 : out std_logic_vector(6 downto 0);
		hex6 : out std_logic_vector(6 downto 0);
		hex7 : out std_logic_vector(6 downto 0);

		-- the LEDs (green and red)
		ledg : out std_logic_vector(8 downto 0);
		ledr : out std_logic_vector(17 downto 0);
		
		-- LCD interface
		nclk    : out std_logic;
		hd      : out std_logic;
		vd      : out std_logic;
		den     : out std_logic;
		r       : out std_logic_vector(7 downto 0);
		g       : out std_logic_vector(7 downto 0);
		b       : out std_logic_vector(7 downto 0);
		grest   : out std_logic;
		
		-- UART
		rx : in std_logic;
		tx : out std_logic;
		
		-- bidir data wire to N64 controller
		n64_data : inout std_logic;
		
		-- audio 
		wm8731_xck     : out std_logic;
		wm8731_sdat : inout std_logic;
		wm8731_sclk : inout std_logic;
		wm8731_dacdat  : out std_logic;
		wm8731_daclrck : out std_logic;
		wm8731_bclk    : out std_logic;
		
		-- emulated n64 data port
		n64_data_emulated : inout std_logic;
		
		-- charcter LCD 
		lcd_data     : inout std_logic_vector(7 downto 0);
		lcd_en       : out std_logic;
		lcd_rw       : out std_logic;
		lcd_rs       : out std_logic;
		lcd_on       : out std_logic
	);
end entity;


architecture arch of remote_bridge_top is

	component top is
		generic (
			ENABLE_DISPLAY_DEBUG_INTERFACE : boolean := false
		);
		port (
			clk : in std_logic;
			keys : in std_logic_vector(3 downto 0);
			switches : in std_logic_vector(17 downto 0);
			hex0 : out std_logic_vector(6 downto 0);
			hex1 : out std_logic_vector(6 downto 0);
			hex2 : out std_logic_vector(6 downto 0);
			hex3 : out std_logic_vector(6 downto 0);
			hex4 : out std_logic_vector(6 downto 0);
			hex5 : out std_logic_vector(6 downto 0);
			hex6 : out std_logic_vector(6 downto 0);
			hex7 : out std_logic_vector(6 downto 0);
			ledg : out std_logic_vector(8 downto 0);
			ledr : out std_logic_vector(17 downto 0);
			nclk : out std_logic;
			hd : out std_logic;
			vd : out std_logic;
			den : out std_logic;
			r : out std_logic_vector(7 downto 0);
			g : out std_logic_vector(7 downto 0);
			b : out std_logic_vector(7 downto 0);
			grest : out std_logic;
			rx : in std_logic;
			tx : out std_logic;
			n64_data : inout std_logic;
			dbg_char_lcd_clk : out std_logic;
			dbg_char_lcd_res_n : out std_logic;
			dbg_char_lcd_instr_wr : in std_logic;
			dbg_char_lcd_instr : in std_logic_vector(3 downto 0);
			dbg_char_lcd_instr_data : in std_logic_vector(15 downto 0);
			dbg_char_lcd_instr_result : out std_logic_vector(15 downto 0);
			dbg_char_lcd_busy : out std_logic;
			wm8731_xck : out std_logic;
			wm8731_sdat : inout std_logic;
			wm8731_sclk : inout std_logic;
			wm8731_dacdat : out std_logic;
			wm8731_daclrck : out std_logic;
			wm8731_bclk : out std_logic;
			lcd_data : inout std_logic_vector(7 downto 0);
			lcd_en : out std_logic;
			lcd_rw : out std_logic;
			lcd_rs : out std_logic;
			lcd_on : out std_logic
		);
	end component;

	signal remote_bridge_switches : std_logic_vector(17 downto 0);
	signal remote_bridge_keys : std_logic_vector(3 downto 0);
	
	signal top_ledr : std_logic_vector(17 downto 0);
	signal top_ledg : std_logic_vector(8 downto 0);
	signal top_hex0 : std_logic_vector(6 downto 0);
	signal top_hex1 : std_logic_vector(6 downto 0);
	signal top_hex2 : std_logic_vector(6 downto 0);
	signal top_hex3 : std_logic_vector(6 downto 0);
	signal top_hex4 : std_logic_vector(6 downto 0);
	signal top_hex5 : std_logic_vector(6 downto 0);
	signal top_hex6 : std_logic_vector(6 downto 0);
	signal top_hex7 : std_logic_vector(6 downto 0);
	
	signal top_lcd_en : std_logic;
	signal top_lcd_rw : std_logic;
	signal top_lcd_rs : std_logic;
	
	signal char_lcd_clk          : std_logic;
	signal char_lcd_res_n        : std_logic;
	signal char_lcd_instr_wr     : std_logic;
	signal char_lcd_instr        : std_logic_vector(3 downto 0);
	signal char_lcd_instr_data   : std_logic_vector(15 downto 0);
	signal char_lcd_instr_result : std_logic_vector(15 downto 0);
	signal char_lcd_busy         : std_logic;
begin
	
	hex0 <= top_hex0;
	hex1 <= top_hex1;
	hex2 <= top_hex2;
	hex3 <= top_hex3;
	hex4 <= top_hex4;
	hex5 <= top_hex5;
	hex6 <= top_hex6;
	hex7 <= top_hex7;
	ledg <= top_ledg;
	ledr <= top_ledr;
	lcd_en <= top_lcd_en;
	lcd_rw <= top_lcd_rw;
	lcd_rs <= top_lcd_rs;
	
	
	remote_bridge_inst : remote_bridge
	port map (
		clk      => clk,
		res_n    => keys(0),
		switches => remote_bridge_switches,
		keys     => remote_bridge_keys,
		n64_data => n64_data_emulated,
		hex0           => top_hex0,
		hex1           => top_hex1,
		hex2           => top_hex2,
		hex3           => top_hex3,
		hex4           => top_hex4,
		hex5           => top_hex5,
		hex6           => top_hex6,
		hex7           => top_hex7,
		ledg           => top_ledg,
		ledr           => top_ledr,
		char_lcd_busy         => char_lcd_busy,
		char_lcd_clk          => char_lcd_clk,
		char_lcd_instr        => char_lcd_instr,
		char_lcd_instr_data   => char_lcd_instr_data,
		char_lcd_instr_result => char_lcd_instr_result,
		char_lcd_instr_wr     => char_lcd_instr_wr,
		char_lcd_res_n        => char_lcd_res_n,
		lcd_data     => lcd_data,
		lcd_en       => top_lcd_en,
		lcd_rw       => top_lcd_rw,
		lcd_rs       => top_lcd_rs
	);

	top_inst : top
	generic map (
		ENABLE_DISPLAY_DEBUG_INTERFACE => true
	)
	port map (
		clk            => clk,
		keys           => remote_bridge_keys,
		switches       => remote_bridge_switches,
		hex0           => top_hex0,
		hex1           => top_hex1,
		hex2           => top_hex2,
		hex3           => top_hex3,
		hex4           => top_hex4,
		hex5           => top_hex5,
		hex6           => top_hex6,
		hex7           => top_hex7,
		ledg           => top_ledg,
		ledr           => top_ledr,
		nclk           => nclk,
		hd             => hd,
		vd             => vd,
		den            => den,
		r              => r,
		g              => g,
		b              => b,
		grest          => grest,
		rx             => rx,
		tx             => tx,
		n64_data       => n64_data,
		dbg_char_lcd_busy         => char_lcd_busy,
		dbg_char_lcd_clk          => char_lcd_clk,
		dbg_char_lcd_instr        => char_lcd_instr,
		dbg_char_lcd_instr_data   => char_lcd_instr_data,
		dbg_char_lcd_instr_result => char_lcd_instr_result,
		dbg_char_lcd_instr_wr     => char_lcd_instr_wr,
		dbg_char_lcd_res_n        => char_lcd_res_n,
		wm8731_xck     => wm8731_xck,
		wm8731_sdat    => wm8731_sdat,
		wm8731_sclk    => wm8731_sclk,
		wm8731_dacdat  => wm8731_dacdat,
		wm8731_daclrck => wm8731_daclrck,
		wm8731_bclk    => wm8731_bclk,
		lcd_data => lcd_data,
		lcd_en   => top_lcd_en,
		lcd_rw   => top_lcd_rw,
		lcd_rs   => top_lcd_rs,
		lcd_on   => lcd_on
	);
	
end architecture;
