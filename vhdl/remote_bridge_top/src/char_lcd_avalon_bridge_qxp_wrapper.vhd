-- altera vhdl_input_version vhdl_2008
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity char_lcd_avalon_bridge_qxp_wrapper is
	port (
		clk   : in  std_logic;
		res_n : in  std_logic;

		address   : in std_logic_vector(1 downto 0);
		writedata : in std_logic_vector(31 downto 0);
		readdata  : out std_logic_vector(31 downto 0);
		write     : in std_logic;
		read      : in std_logic;

		char_lcd_clk          : in std_logic;
		char_lcd_res_n        : in std_logic;
		char_lcd_instr_wr     : out std_logic;
		char_lcd_instr        : out std_logic_vector(3 downto 0);
		char_lcd_instr_data   : out std_logic_vector(15 downto 0);
		char_lcd_instr_result : in std_logic_vector(15 downto 0);
		char_lcd_busy         : in std_logic;
		
		lcd_data     : in std_logic_vector(7 downto 0);
		lcd_en       : in std_logic;
		lcd_rw       : in std_logic;
		lcd_rs       : in std_logic
	);
end entity;

architecture arch of char_lcd_avalon_bridge_qxp_wrapper is
	component char_lcd_avalon_bridge is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			address : in std_logic_vector(1 downto 0);
			writedata : in std_logic_vector(31 downto 0);
			readdata : out std_logic_vector(31 downto 0);
			write : in std_logic;
			read : in std_logic;
			char_lcd_clk : in std_logic;
			char_lcd_res_n : in std_logic;
			char_lcd_instr_wr : out std_logic;
			char_lcd_instr : out std_logic_vector(3 downto 0);
			char_lcd_instr_data : out std_logic_vector(15 downto 0);
			char_lcd_instr_result : in std_logic_vector(15 downto 0);
			char_lcd_busy : in std_logic;
			lcd_data : in std_logic_vector(7 downto 0);
			lcd_en : in std_logic;
			lcd_rw : in std_logic;
			lcd_rs : in std_logic
		);
	end component;

begin
	char_lcd_avalon_bridge_inst : char_lcd_avalon_bridge
	port map (
		clk                   => clk,
		res_n                 => res_n,
		address               => address,
		writedata             => writedata,
		readdata              => readdata,
		write                 => write,
		read                  => read,
		char_lcd_clk          => char_lcd_clk,
		char_lcd_res_n        => char_lcd_res_n,
		char_lcd_instr_wr     => char_lcd_instr_wr,
		char_lcd_instr        => char_lcd_instr,
		char_lcd_instr_data   => char_lcd_instr_data,
		char_lcd_instr_result => char_lcd_instr_result,
		char_lcd_busy         => char_lcd_busy,
		lcd_data              => lcd_data,
		lcd_en                => lcd_en,
		lcd_rw                => lcd_rw,
		lcd_rs                => lcd_rs
	);


end architecture;
