

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.n64_controller_emulator_pkg.all;
use work.n64_controller_pkg.all;

entity remote_bridge is
	port (
		clk   : in std_logic;
		res_n    : in std_logic;
		
		switches : out std_logic_vector(17 downto 0);
		keys     : out std_logic_vector(3 downto 0);
		n64_data : inout std_logic;
		ledr : in std_logic_vector(17 downto 0);
		ledg : in std_logic_vector(8 downto 0);
		hex0 : in std_logic_vector(6 downto 0);
		hex1 : in std_logic_vector(6 downto 0);
		hex2 : in std_logic_vector(6 downto 0);
		hex3 : in std_logic_vector(6 downto 0);
		hex4 : in std_logic_vector(6 downto 0);
		hex5 : in std_logic_vector(6 downto 0);
		hex6 : in std_logic_vector(6 downto 0);
		hex7 : in std_logic_vector(6 downto 0);
		
		char_lcd_clk          : in std_logic;
		char_lcd_res_n        : in std_logic;
		char_lcd_instr_wr     : out std_logic;
		char_lcd_instr        : out std_logic_vector(3 downto 0);
		char_lcd_instr_data   : out std_logic_vector(15 downto 0);
		char_lcd_instr_result : in std_logic_vector(15 downto 0);
		char_lcd_busy         : in std_logic;
		
		lcd_data     : in std_logic_vector(7 downto 0);
		lcd_en       : in std_logic;
		lcd_rw       : in std_logic;
		lcd_rs       : in std_logic
	);
end entity;


architecture arch of remote_bridge is

 component nios_system is
        port (
            clk_clk                                   : in  std_logic                     := 'X';             -- clk
            hex0_3_export                             : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            hex4_7_export                             : in  std_logic_vector(31 downto 0) := (others => 'X'); -- export
            keys_export                               : out std_logic_vector(3 downto 0);                     -- export
            ledg_export                               : in  std_logic_vector(8 downto 0)  := (others => 'X'); -- export
            ledr_export                               : in  std_logic_vector(17 downto 0) := (others => 'X'); -- export
            n64_controller_state_export               : out std_logic_vector(31 downto 0);                    -- export
            reset_reset_n                             : in  std_logic                     := 'X';             -- reset_n
            switches_export                           : out std_logic_vector(17 downto 0);                    -- export
            char_lcd_instr_port_char_lcd_busy         : in  std_logic                     := 'X';             -- char_lcd_busy
            char_lcd_instr_port_char_lcd_clk          : in  std_logic                     := 'X';             -- char_lcd_clk
            char_lcd_instr_port_char_lcd_instr        : out std_logic_vector(3 downto 0);                     -- char_lcd_instr
            char_lcd_instr_port_char_lcd_instr_data   : out std_logic_vector(15 downto 0);                    -- char_lcd_instr_data
            char_lcd_instr_port_char_lcd_instr_result : in  std_logic_vector(15 downto 0) := (others => 'X'); -- char_lcd_instr_result
            char_lcd_instr_port_char_lcd_instr_wr     : out std_logic;                                        -- char_lcd_instr_wr
            char_lcd_instr_port_char_lcd_res_n        : in  std_logic                     := 'X';             -- char_lcd_res_n
            char_lcd_lcd_cntrl_signals_lcd_data       : in  std_logic_vector(7 downto 0)  := (others => 'X'); -- lcd_data
            char_lcd_lcd_cntrl_signals_lcd_en         : in  std_logic                     := 'X';             -- lcd_en
            char_lcd_lcd_cntrl_signals_lcd_rs         : in  std_logic                     := 'X';             -- lcd_rs
            char_lcd_lcd_cntrl_signals_lcd_rw         : in  std_logic                     := 'X'              -- lcd_rw
        );
    end component nios_system;


	signal button_state : std_logic_vector(31 downto 0);
begin

	u0 : component nios_system
	port map (
		switches_export             => switches,
		keys_export                 => keys,
		n64_controller_state_export => button_state,
		reset_reset_n               => res_n,
		clk_clk                     => clk,
		ledr_export                 => ledr,
		ledg_export                 => ledg,
		hex0_3_export(7)                   => '0',
		hex0_3_export(15)                  => '0',
		hex0_3_export(23)                  => '0',
		hex0_3_export(31)                  => '0',
		hex4_7_export(7)                   => '0',
		hex4_7_export(15)                  => '0',
		hex4_7_export(23)                  => '0',
		hex4_7_export(31)                  => '0',
		hex0_3_export(6 downto 0)          => hex0,
		hex0_3_export(14 downto 8)         => hex1,
		hex0_3_export(22 downto 16)        => hex2,
		hex0_3_export(30 downto 24)        => hex3,
		hex4_7_export(6 downto 0)          => hex4,
		hex4_7_export(14 downto 8)         => hex5,
		hex4_7_export(22 downto 16)        => hex6,
		hex4_7_export(30 downto 24)        => hex7,
		char_lcd_instr_port_char_lcd_busy         => char_lcd_busy,
		char_lcd_instr_port_char_lcd_clk          => char_lcd_clk,
		char_lcd_instr_port_char_lcd_instr        => char_lcd_instr,
		char_lcd_instr_port_char_lcd_instr_data   => char_lcd_instr_data,
		char_lcd_instr_port_char_lcd_instr_result => char_lcd_instr_result,
		char_lcd_instr_port_char_lcd_instr_wr     => char_lcd_instr_wr,
		char_lcd_instr_port_char_lcd_res_n        => char_lcd_res_n,
		char_lcd_lcd_cntrl_signals_lcd_data       => lcd_data,
		char_lcd_lcd_cntrl_signals_lcd_en         => lcd_en,
		char_lcd_lcd_cntrl_signals_lcd_rs         => lcd_rs,
		char_lcd_lcd_cntrl_signals_lcd_rw         => lcd_rw
	);

	n64_controller_emulator_inst : n64_controller_emulator
	port map(
		clk  => clk,
		res_n => res_n,
		data  => n64_data,
		button_state.btn_up     => button_state(4),
		button_state.btn_down   => button_state(5),
		button_state.btn_left   => button_state(6),
		button_state.btn_right  => button_state(7),
		button_state.btn_c_up   => button_state(12),
		button_state.btn_c_down => button_state(13),
		button_state.btn_c_left => button_state(14),
		button_state.btn_c_right=> button_state(15),
		button_state.btn_start  => button_state(3),
		button_state.btn_z      => button_state(2),
		button_state.btn_l      => button_state(10),
		button_state.btn_r      => button_state(11),
		button_state.btn_a      => button_state(0),
		button_state.btn_b      => button_state(1),
		button_state.as_x       => button_state(23 downto 16),
		button_state.as_y       => button_state(31 downto 24)
	);
end architecture;



