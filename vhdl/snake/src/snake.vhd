
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.snake_pkg.all;
use work.textmode_controller_pkg.all;

entity snake is
	port (
		clk : in std_logic;
		res_n : in std_logic;

		tm_instr_wr     : out std_logic;
		tm_instr        : out std_logic_vector(3 downto 0);
		tm_instr_data   : out std_logic_vector(15 downto 0);
		tm_instr_result : in std_logic_vector(15 downto 0);
		tm_busy         : in std_logic;

		color : in std_logic_vector(7 downto 0);

		init				: in std_logic;
		init_head_position	: in vec2d_t;
		init_head_direction	: in direction_t;
		init_body_length	: in std_logic_vector(3 downto 0);

		head_position		: out vec2d_t;

		move_head : in std_logic;
		movement_direction : in direction_t;

		move_tail : in std_logic;

		done :  out std_logic
	);
end entity;


architecture arch of snake is

	type state_t is (
		IDLE,
		COMPLETE,
		-- snake initialization states
		PLACE_HEAD, PLACE_HEAD_CHAR,
		PLACE_BODY, PLACE_BODY_CHARACTER,
		PLACE_TAIL, PLACE_TAIL_CHAR,
		-- move head states
		REPLACE_OLD_HEAD_MOVE_CURSOR, REPLACE_OLD_HEAD_SET_CHAR,
		DRAW_NEW_HEAD_MOVE_CURSOR, DRAW_NEW_HEAD_SET_CHAR,
		-- move tail states
		DELETE_OLD_TAIL_SET_CURSOR, DELETE_OLD_TAIL_SET_CHAR,
		CALCULATE_NEW_TAIL_DIRECTION_SET_CURSOR,
		CALCULATE_NEW_TAIL_DIRECTION_GET_CHAR,
		CALCULATE_NEW_TAIL_DIRECTION,
		DRAW_NEW_TAIL_SET_CHAR
	);

	signal state : state_t;
	signal cur_position : vec2d_t;
	signal cur_length : std_logic_vector(3 downto 0);

	signal the_snake : snake_position_t;
begin

	head_position <= the_snake.head_position;


	sync : process(clk, res_n)
	begin
		if (res_n = '0') then
			state <= IDLE;
			cur_length <= (others=>'0');
			cur_position <= NULL_VEC;
			the_snake <= NULL_SNAKE_POSITION;

			tm_instr_wr <= '0';
			tm_instr <= INSTR_NOP;
			tm_instr_data <= (others=>'0');
			done <= '0';
		elsif (rising_edge(clk)) then

			tm_instr_wr <= '0';
			done <= '0';

			case state is
				when IDLE =>
					if (init = '1') then
						state <= PLACE_HEAD;
						cur_length <= (others=>'0');
						cur_position <= init_head_position;
						the_snake.head_direction <= init_head_direction;
						the_snake.tail_direction <= init_head_direction;
						the_snake.head_position <= init_head_position;
					elsif (move_head = '1') then
						state <= REPLACE_OLD_HEAD_MOVE_CURSOR;
					elsif (move_tail = '1') then
						state <= DELETE_OLD_TAIL_SET_CURSOR;
					end if;

				------------------------------------------------
				--            SNAKE INITIALIZATION            --
				------------------------------------------------
				when PLACE_HEAD =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CURSOR_POSITION;
						tm_instr_data <= vec2d_to_txt_cntrl_data(cur_position);
						state <= PLACE_HEAD_CHAR;
					end if;

				when PLACE_HEAD_CHAR =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CHAR;
						tm_instr_data <= color & get_head_character_from_direction(init_head_direction);
						state <= PLACE_BODY;
						cur_position <= move_vec2d(cur_position, not init_head_direction);
					end if;

				when PLACE_BODY =>
					if(unsigned(cur_length) = unsigned(init_body_length)) then
						state <= PLACE_TAIL;
					else
						if (tm_busy = '0' and tm_instr_wr = '0') then
							tm_instr_wr <= '1';
							tm_instr <= INSTR_SET_CURSOR_POSITION;
							tm_instr_data <= vec2d_to_txt_cntrl_data(cur_position);
							state <= PLACE_BODY_CHARACTER;
						end if;
					end if;

				when PLACE_BODY_CHARACTER =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CHAR;
						if (init_head_direction = UP or init_head_direction = DOWN) then
							tm_instr_data <= color & SNAKE_BODY_VERTICAL;
						else
							tm_instr_data <= color & SNAKE_BODY_HORIZONTAL;
						end if;
						cur_length <= std_logic_vector(unsigned(cur_length) + 1);
						cur_position <= move_vec2d(cur_position, not init_head_direction);
						state <= PLACE_BODY;
					end if;

				when PLACE_TAIL =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CURSOR_POSITION;
						tm_instr_data <= vec2d_to_txt_cntrl_data(cur_position);
						state <= PLACE_TAIL_CHAR;

						the_snake.tail_position <= cur_position;
					end if;

				when PLACE_TAIL_CHAR =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CHAR;
						tm_instr_data <= color & get_tail_character_from_direction(init_head_direction);
						state <= COMPLETE;
					end if;

				------------------------------------------------
				--              HEAD MOVEMENT                 --
				------------------------------------------------
				when REPLACE_OLD_HEAD_MOVE_CURSOR =>
					if (tm_busy = '0' and tm_instr_wr='0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CURSOR_POSITION;
						tm_instr_data <= vec2d_to_txt_cntrl_data(the_snake.head_position);
						state <= REPLACE_OLD_HEAD_SET_CHAR;
					end if;

				when REPLACE_OLD_HEAD_SET_CHAR =>
					if (tm_busy = '0' and tm_instr_wr='0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CHAR;
						tm_instr_data <= color & get_body_symbol_for_head_replacement(the_snake.head_direction, movement_direction);
						state <= DRAW_NEW_HEAD_MOVE_CURSOR;

						--move head to new position
						the_snake.head_position <= move_vec2d_and_wrap(the_snake.head_position, movement_direction, COLUMN_COUNT-1, ROW_COUNT-1);
						the_snake.head_direction <= movement_direction;
					end if;

				when DRAW_NEW_HEAD_MOVE_CURSOR =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CURSOR_POSITION;
						tm_instr_data <= vec2d_to_txt_cntrl_data(the_snake.head_position);
						state <= DRAW_NEW_HEAD_SET_CHAR;
					end if;

				when DRAW_NEW_HEAD_SET_CHAR =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CHAR;
						tm_instr_data <= color & get_head_character_from_direction(the_snake.head_direction);
						state <= COMPLETE;
					end if;

				------------------------------------------------
				--              TAIL MOVEMENT                 --
				------------------------------------------------
				when DELETE_OLD_TAIL_SET_CURSOR =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CURSOR_POSITION;
						tm_instr_data <= vec2d_to_txt_cntrl_data(the_snake.tail_position);
						state <= DELETE_OLD_TAIL_SET_CHAR;
					end if;

				when DELETE_OLD_TAIL_SET_CHAR =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CHAR;
						tm_instr_data <= (others=>'0');
						state <= CALCULATE_NEW_TAIL_DIRECTION_SET_CURSOR;

						--calculate new tail position
						the_snake.tail_position <= move_vec2d_and_wrap(
							the_snake.tail_position,
							the_snake.tail_direction,
							COLUMN_COUNT-1,
							ROW_COUNT-1
						);
					end if;

				when CALCULATE_NEW_TAIL_DIRECTION_SET_CURSOR =>
					if (tm_busy = '0' and tm_instr_wr='0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CURSOR_POSITION;
						tm_instr_data <= vec2d_to_txt_cntrl_data(the_snake.tail_position);
						state <= CALCULATE_NEW_TAIL_DIRECTION_GET_CHAR;
					end if;

				when CALCULATE_NEW_TAIL_DIRECTION_GET_CHAR =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_GET_CHAR;
						state <= CALCULATE_NEW_TAIL_DIRECTION;
					end if;

				when CALCULATE_NEW_TAIL_DIRECTION =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						the_snake.tail_direction <= get_new_tail_direction(the_snake.tail_direction, tm_instr_result(7 downto 0));
						state <= DRAW_NEW_TAIL_SET_CHAR;
					end if;

				when DRAW_NEW_TAIL_SET_CHAR =>
					if (tm_busy = '0' and tm_instr_wr = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CHAR;
						tm_instr_data <= color & get_tail_character_from_direction(the_snake.tail_direction);
						state <= COMPLETE;
					end if;


				when COMPLETE =>
					done <= '1';
					state <= IDLE;

				when others =>
					null;

			end case;


		end if;
	end process;


end architecture;
