-- altera vhdl_input_version vhdl_2008
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity display_cmd_mux is
	port (
		clk   : in  std_logic;
		res_n : in  std_logic;

		port_a_instr_wr     : in std_logic;
		port_a_instr        : in std_logic_vector(3 downto 0);
		port_a_instr_data   : in std_logic_vector(15 downto 0);
		port_a_instr_result : out std_logic_vector(15 downto 0);
		port_a_busy         : out std_logic;
		
		port_b_instr_wr     : in std_logic;
		port_b_instr        : in std_logic_vector(3 downto 0);
		port_b_instr_data   : in std_logic_vector(15 downto 0);
		port_b_instr_result : out std_logic_vector(15 downto 0);
		port_b_busy         : out std_logic;
		
		instr_wr     : out std_logic;
		instr        : out std_logic_vector(3 downto 0);
		instr_data   : out std_logic_vector(15 downto 0);
		instr_result : in std_logic_vector(15 downto 0);
		busy         : in std_logic
	);
end entity;


architecture arch of display_cmd_mux is
	signal port_a_instr_buffer : std_logic_vector(3 downto 0);
	signal port_b_instr_buffer : std_logic_vector(3 downto 0);
	signal port_a_instr_data_buffer : std_logic_vector(15 downto 0);
	signal port_b_instr_data_buffer : std_logic_vector(15 downto 0);
	
	type state_t is (IDLE, RESET_WR, WAIT_FOR_BUSY_LOW);
	signal state : state_t;
	
	signal port_a_request : std_logic;
	signal port_b_request : std_logic;
	signal port_served : std_logic;
	
	constant PORT_A : std_logic := '0';
	constant PORT_B : std_logic := '1';
begin
	process(clk, res_n)
	begin
		if (res_n = '0') then
			port_a_instr_result <= (others=>'0');
			port_b_instr_result <= (others=>'0');
			port_a_busy <= '0';
			port_b_busy <= '0';
			state <= IDLE;
			port_served <= '0';
			instr <= (others=>'0');
			instr_data <= (others=>'0');
			instr_wr <= '0';
			port_a_request <= '0';
			port_b_request <= '0';
		elsif(rising_edge(clk)) then
			
			if (port_a_instr_wr = '1') then
				port_a_request <= '1';
				port_a_busy <= '1';
				port_a_instr_buffer <= port_a_instr;
				port_a_instr_data_buffer <= port_a_instr_data;
			end if;
			
			if (port_b_instr_wr = '1') then
				port_b_request <= '1';
				port_b_busy <= '1';
				port_b_instr_buffer <= port_b_instr;
				port_b_instr_data_buffer <= port_b_instr_data;
			end if;
			
			case state is
				when IDLE =>
					--initially busy may not be zero
					if (busy = '0') then
						if(port_a_request = '1') then
							state <= RESET_WR;
							instr <= port_a_instr_buffer;
							instr_data <= port_a_instr_data_buffer;
							instr_wr <= '1';
							port_served <= PORT_A;
						elsif(port_b_request = '1') then
							state <= RESET_WR;
							instr <= port_b_instr_buffer;
							instr_data <= port_b_instr_data_buffer;
							instr_wr <= '1';
							port_served <= PORT_B;
						end if;
					end if;
				
				when RESET_WR =>
					instr_wr <= '0';
					state <= WAIT_FOR_BUSY_LOW;
					
				when WAIT_FOR_BUSY_LOW =>
					if (busy = '0') then
						if (port_served = PORT_A) then
							port_a_busy <= '0';
							port_a_instr_result <= instr_result;
							port_a_request <= '0';
						elsif (port_served = PORT_B) then
							port_b_busy <= '0';
							port_b_instr_result <= instr_result;
							port_b_request <= '0';
						end if;
						state <= IDLE;
					end if;
			end case;
			
			
		end if;
	end process;

end architecture;
