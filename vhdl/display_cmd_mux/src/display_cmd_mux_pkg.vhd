library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package display_cmd_mux_pkg is

	component display_cmd_mux is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			port_a_instr_wr : in std_logic;
			port_a_instr : in std_logic_vector(3 downto 0);
			port_a_instr_data : in std_logic_vector(15 downto 0);
			port_a_instr_result : out std_logic_vector(15 downto 0);
			port_a_busy : out std_logic;
			port_b_instr_wr : in std_logic;
			port_b_instr : in std_logic_vector(3 downto 0);
			port_b_instr_data : in std_logic_vector(15 downto 0);
			port_b_instr_result : out std_logic_vector(15 downto 0);
			port_b_busy : out std_logic;
			instr_wr : out std_logic;
			instr : out std_logic_vector(3 downto 0);
			instr_data : out std_logic_vector(15 downto 0);
			instr_result : in std_logic_vector(15 downto 0);
			busy : in std_logic
		);
	end component;
end package;

