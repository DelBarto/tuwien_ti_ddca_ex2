library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is
end entity;

architecture bench of top_tb is

	component top is
		port (
			clk : in std_logic;
			keys : in std_logic_vector(3 downto 0);
			switches : in std_logic_vector(17 downto 0);
			hex0 : out std_logic_vector(6 downto 0);
			hex1 : out std_logic_vector(6 downto 0);
			hex2 : out std_logic_vector(6 downto 0);
			hex3 : out std_logic_vector(6 downto 0);
			hex4 : out std_logic_vector(6 downto 0);
			hex5 : out std_logic_vector(6 downto 0);
			hex6 : out std_logic_vector(6 downto 0);
			hex7 : out std_logic_vector(6 downto 0);
			ledg : out std_logic_vector(8 downto 0);
			ledr : out std_logic_vector(17 downto 0);
			nclk : out std_logic;
			hd : out std_logic;
			vd : out std_logic;
			den : out std_logic;
			r : out std_logic_vector(7 downto 0);
			g : out std_logic_vector(7 downto 0);
			b : out std_logic_vector(7 downto 0);
			grest : out std_logic;
			rx : in std_logic;
			tx : out std_logic;
			n64_data : inout std_logic;
			wm8731_xck     : out std_logic;
			wm8731_sdat : inout std_logic;
			wm8731_sclk : inout std_logic;
			wm8731_dacdat  : out std_logic;
			wm8731_daclrck : out std_logic;
			wm8731_bclk    : out std_logic
		);
	end component;

	signal clk : std_logic;
	signal keys : std_logic_vector(3 downto 0);
	signal switches : std_logic_vector(17 downto 0);
	signal hex0 : std_logic_vector(6 downto 0);
	signal hex1 : std_logic_vector(6 downto 0);
	signal hex2 : std_logic_vector(6 downto 0);
	signal hex3 : std_logic_vector(6 downto 0);
	signal hex4 : std_logic_vector(6 downto 0);
	signal hex5 : std_logic_vector(6 downto 0);
	signal hex6 : std_logic_vector(6 downto 0);
	signal hex7 : std_logic_vector(6 downto 0);
	signal ledg : std_logic_vector(8 downto 0);
	signal ledr : std_logic_vector(17 downto 0);
	signal nclk : std_logic;
	signal hd : std_logic;
	signal vd : std_logic;
	signal den : std_logic;
	signal r : std_logic_vector(7 downto 0);
	signal g : std_logic_vector(7 downto 0);
	signal b : std_logic_vector(7 downto 0);
	signal grest : std_logic;
	signal rx : std_logic;
	signal tx : std_logic;
	signal n64_data : std_logic;
	signal wm8731_xck : std_logic;
	signal wm8731_sdat : std_logic;
	signal wm8731_sclk : std_logic;
	signal wm8731_dacdat : std_logic;
	signal wm8731_daclrck : std_logic;
	signal wm8731_bclk : std_logic;


	signal res_n : std_logic;

	constant CLK_PERIOD : time := 20 ns;
	signal stop_clock : boolean := false;
begin

	keys(0) <= res_n;
	rx <= tx; --loop back

	uut : top
		port map (
			clk      => clk,
			keys     => keys,
			switches => switches,
			hex0     => hex0,
			hex1     => hex1,
			hex2     => hex2,
			hex3     => hex3,
			hex4     => hex4,
			hex5     => hex5,
			hex6     => hex6,
			hex7     => hex7,
			ledg     => ledg,
			ledr     => ledr,
			nclk     => nclk,
			hd       => hd,
			vd       => vd,
			den      => den,
			r        => r,
			g        => g,
			b        => b,
			grest    => grest,
			rx       => rx,
			tx       => tx,
			n64_data => n64_data,
			wm8731_xck     => wm8731_xck,
			wm8731_sdat    => wm8731_sdat,
			wm8731_sclk    => wm8731_sclk,
			wm8731_dacdat  => wm8731_dacdat,
			wm8731_daclrck => wm8731_daclrck,
			wm8731_bclk    => wm8731_bclk
		);

	stimulus : process
	begin

		switches <= (others=>'0');
		res_n <= '0';
		keys(3 downto 1) <= (others=>'0');

		wait for 5.5 * CLK_PERIOD;
		wait until rising_edge(clk);
		res_n <= '1';

		wait until rising_edge(clk);
		report "starting simulation";

		switches(17) <= '1';

		--cycle through all the game states using the fake N64 controller
		switches(13) <= '1';
		switches(12) <= '1';
		wait for 10 * CLK_PERIOD;

		switches(13) <= '0';
		switches(12) <= '1';
		wait for 10 * CLK_PERIOD;

		switches(13) <= '1';
		switches(12) <= '0';
		wait for 10 * CLK_PERIOD;

		switches(13) <= '0';
		switches(12) <= '0';
		wait for 10 * CLK_PERIOD;


		wait until falling_edge(vd);
		report "simulation Done";
		stop_clock <= true;

		wait;
	end process;

	generate_clk : process
	begin
		while not stop_clock loop
			clk <= '0', '1' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;

end architecture;
