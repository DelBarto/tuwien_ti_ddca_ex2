vlib work
vmap work work


onerror {resume}
#add wave -noupdate -divider Signals
add wave -noupdate /top_tb/nclk
add wave -noupdate /top_tb/vd
add wave -noupdate /top_tb/hd
add wave -noupdate /top_tb/den
add wave -noupdate /top_tb/r
add wave -noupdate /top_tb/g
add wave -noupdate /top_tb/b
add wave -noupdate -divider LCD DISPLAY
add wave -noupdate /top_tb/uut/lcd_data
add wave -noupdate /top_tb/uut/lcd_en
add wave -noupdate /top_tb/uut/lcd_rw
add wave -noupdate /top_tb/uut/lcd_rs
add wave -noupdate /top_tb/uut/lcd_on
