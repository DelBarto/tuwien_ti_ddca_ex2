library ieee;
use ieee.std_logic_1164.all;

-- import all required packages
use work.sync_pkg.all;
use work.n64_controller_pkg.all;
use work.textmode_controller_pkg.all;
use work.snake_pkg.all;
use work.serial_port_pkg.all;
use work.audio_cntrl_pkg.all;
use work.snake_game_pkg.all;
use work.ssd_cntrl_pkg.all;
use work.display_cmd_mux_pkg.all; --Exercise II
use work.char_lcd_cntrl_pkg.all; --Exercise II


entity top is
	generic (
		ENABLE_DISPLAY_DEBUG_INTERFACE : boolean := false
	);
	port (
		--50 MHz clock input
		clk      : in  std_logic;

		-- push buttons and switches
		keys     : in std_logic_vector(3 downto 0);
		switches : in std_logic_vector(17 downto 0);

		--Seven segment displays
		hex0 : out std_logic_vector(6 downto 0);
		hex1 : out std_logic_vector(6 downto 0);
		hex2 : out std_logic_vector(6 downto 0);
		hex3 : out std_logic_vector(6 downto 0);
		hex4 : out std_logic_vector(6 downto 0);
		hex5 : out std_logic_vector(6 downto 0);
		hex6 : out std_logic_vector(6 downto 0);
		hex7 : out std_logic_vector(6 downto 0);

		-- the LEDs (green and red)
		ledg : out std_logic_vector(8 downto 0);
		ledr : out std_logic_vector(17 downto 0);

		-- LCD interface
		nclk    : out std_logic;
		hd      : out std_logic;
		vd      : out std_logic;
		den     : out std_logic;
		r       : out std_logic_vector(7 downto 0);
		g       : out std_logic_vector(7 downto 0);
		b       : out std_logic_vector(7 downto 0);
		grest   : out std_logic;

		-- UART
		rx : in std_logic;
		tx : out std_logic;

		-- bidir data wire to N64 controller
		n64_data : inout std_logic;

		--char LCD command debug interface
		dbg_char_lcd_clk          : out std_logic;
		dbg_char_lcd_res_n        : out std_logic;
		dbg_char_lcd_instr_wr     : in std_logic := '0';
		dbg_char_lcd_instr        : in std_logic_vector(3 downto 0) := (others=>'0');
		dbg_char_lcd_instr_data   : in std_logic_vector(15 downto 0) := (others=>'0');
		dbg_char_lcd_instr_result : out std_logic_vector(15 downto 0);
		dbg_char_lcd_busy         : out std_logic;

		-- audio
		wm8731_xck     : out std_logic;
		wm8731_sdat : inout std_logic;
		wm8731_sclk : inout std_logic;
		wm8731_dacdat  : out std_logic;
		wm8731_daclrck : out std_logic;
		wm8731_bclk    : out std_logic;

		-- charcter LCD
		lcd_data     : inout std_logic_vector(7 downto 0);
		lcd_en       : out std_logic;
		lcd_rw       : out std_logic;
		lcd_rs       : out std_logic;
		lcd_on       : out std_logic
	);
end entity;



-- TASK 1: add your architecture here!
ARCHITECTURE top_arch OF top IS
CONSTANT SYS_CLK_FREQ			: NATURAL := 25000000;
CONSTANT TX_TIMEOUT_MS			: NATURAL := 10;
CONSTANT SYNC_STAGES			: NATURAL := 2;

SIGNAL sys_clk					: STD_LOGIC;
SIGNAL audio_clk				: STD_LOGIC;
SIGNAL sys_res_n				: STD_LOGIC;
SIGNAL audio_res_n				: STD_LOGIC;
SIGNAL synth_cntrl				: SYNTH_CNTRL_VEC_T(0 TO 1);
SIGNAL cntrl1					: N64_BUTTONS_T;
SIGNAL cntrl2					: N64_BUTTONS_T;
SIGNAL instr_result				: STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL busy						: STD_LOGIC;
SIGNAL vblank					: STD_LOGIC;
SIGNAL instr					: STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL instr_data				: STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL instr_wr					: STD_LOGIC;

SIGNAL char_lcd_instr_wr		: STD_LOGIC;
SIGNAL char_lcd_instr			: STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL char_lcd_instr_data		: STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL char_lcd_instr_result	: STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL char_lcd_busy			: STD_LOGIC;

SIGNAL tx_free					: STD_LOGIC;
SIGNAL rx_empty					: STD_LOGIC;
SIGNAL rx_data					: STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL tx_wr					: STD_LOGIC;
SIGNAL tx_data					: STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL rx_rd					: STD_LOGIC;

SIGNAL fake_n64_btnstate	 	: N64_BUTTONS_T;
SIGNAL real_n64_btnstate	 	: N64_BUTTONS_T;

-- signals for task 2
SIGNAL game_state 				: snake_game_state_t;



COMPONENT pll IS
	PORT
	(
		areset		: IN STD_LOGIC  := '0';
		inclk0		: IN STD_LOGIC  := '0';
		c0				: OUT STD_LOGIC ;
		c1				: OUT STD_LOGIC ;
		locked		: OUT STD_LOGIC
	);
END COMPONENT pll;

COMPONENT uart_n64_bridge IS
		GENERIC (
			TX_TIMEOUT : integer := 500_000
		);
		PORT (
			clk : IN STD_LOGIC;
			res_n : IN STD_LOGIC;
			button_state_in : IN N64_BUTTONS_T;
			button_state_out : OUT N64_BUTTONS_T;
			tx_data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
			tx_wr : OUT STD_LOGIC;
			tx_free : IN STD_LOGIC;
			rx_data : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			rx_rd : OUT STD_LOGIC;
			rx_empty : IN STD_LOGIC
		);
END COMPONENT uart_n64_bridge;

BEGIN

	pll_inst : pll
		PORT MAP(
			inclk0 => clk,
			c0 => sys_clk,
			c1 => audio_clk
		);


	audio_reset_sync : sync
		generic map(
			SYNC_STAGES => SYNC_STAGES,
			RESET_VALUE => '1'
		)
		PORT MAP(
			clk => audio_clk,
			res_n => '1',
			data_in => keys(0),
			data_out => audio_res_n
		);

	sys_reset_sync : sync
		generic map(
			SYNC_STAGES => SYNC_STAGES,
			RESET_VALUE => '1'
		)
		PORT MAP(
			clk => sys_clk,
			res_n => '1',
			data_in => keys(0),
			data_out => sys_res_n
		);


	audio_cntrl_inst : audio_cntrl_2s
		PORT MAP(
			clk => audio_clk,
			res_n => audio_res_n,
			synth_cntrl => synth_cntrl,
			wm8731_xck => wm8731_xck,
			wm8731_dacdat => wm8731_dacdat,
			wm8731_daclrck => wm8731_daclrck,
			wm8731_bclk => wm8731_bclk,
			wm8731_sclk => wm8731_sclk,
			wm8731_sdat => wm8731_sdat
		);


	snake_game_inst : snake_game
		PORT MAP(
			clk 						=> sys_clk,
			res_n 						=> sys_res_n,

			instr_wr					=> instr_wr,
			instr						=> instr,
			instr_data					=> instr_data,
			instr_result				=> instr_result,
			busy						=> busy,
			vblank						=> vblank,

			cntrl1 						=> cntrl1,
			cntrl2 						=> cntrl2,

			synth_cntrl 				=> synth_cntrl,

			char_lcd_instr_wr			=> char_lcd_instr_wr,
			char_lcd_instr				=> char_lcd_instr,
			char_lcd_instr_data			=> char_lcd_instr_data,
			char_lcd_instr_result		=> char_lcd_instr_result,
			char_lcd_busy				=> char_lcd_busy,

			game_state 					=> game_state
		);


	textmode_controller_inst : textmode_controller
		GENERIC MAP(
			CLK_FREQ => SYS_CLK_FREQ
		)
		PORT MAP(
			clk => sys_clk,
			res_n => sys_res_n,
			instr => instr,
			instr_data => instr_data,
			instr_wr => instr_wr,
			instr_result => instr_result,
			busy => busy,
			vblank => vblank,
			nclk => nclk,
			grest => grest,
			vd => vd,
			hd => hd,
			den => den,
			r => r,
			g => g,
			b => b
		);


	n64_cntrl_inst : fake_n64_controller
		GENERIC MAP(
			SYNC_STAGES => SYNC_STAGES
		)
		PORT MAP(
			clk => sys_clk,
			res_n => sys_res_n,
			switches => switches(13 DOWNTO 0),
			button_state => fake_n64_btnstate
		);


	serial_port_inst : serial_port
		PORT MAP(
			clk => sys_clk,
			res_n => sys_res_n,
			tx_wr => tx_wr,
			tx_data => tx_data,
			rx_rd => rx_rd,
			rx => rx,
			tx_free => tx_free,
			tx => tx,
			rx_empty => rx_empty,
			rx_data => rx_data
		);


	uart_n64_bridge_inst : uart_n64_bridge
		GENERIC MAP(
			TX_TIMEOUT => TX_TIMEOUT_MS*SYS_CLK_FREQ/1000
		)
		PORT MAP(
			clk => sys_clk,
			res_n => sys_res_n,
			button_state_in => cntrl1,
			tx_free => tx_free,
			rx_empty => rx_empty,
			rx_data => rx_data,
			button_state_out => cntrl2,
			tx_wr => tx_wr,
			tx_data => tx_data,
			rx_rd => rx_rd
		);

	n64_cntrl_inst_real : n64_controller
		GENERIC MAP(
			CLK_FREQ => SYS_CLK_FREQ, -- edit
			SYNC_STAGES => SYNC_STAGES,
			REFRESH_TIMEOUT => integer(SYS_CLK_FREQ*8/1_000)
		)
		PORT MAP(
			clk => sys_clk,
			res_n => sys_res_n,
			data => n64_data,
			button_state => real_n64_btnstate
		);

	-- for TASK 1
	-- cntrl1 <= fake_n64_btnstate
	-- otherwise: select if fake or real n64_controller is used
	cntrl1 <= fake_n64_btnstate when (switches(17) = '1') else real_n64_btnstate;


	ledr(0)  <= cntrl2.btn_up;
	ledr(1)  <= cntrl2.btn_down;
	ledr(2)  <= cntrl2.btn_left;
	ledr(3)  <= cntrl2.btn_right;
	ledr(4)  <= cntrl2.btn_c_up;
	ledr(5)  <= cntrl2.btn_c_down;
	ledr(6)  <= cntrl2.btn_c_left;
	ledr(7)  <= cntrl2.btn_c_right;
	ledr(8)  <= cntrl2.btn_a;
	ledr(9)  <= cntrl2.btn_b;
	ledr(10) <= cntrl2.btn_start;
	ledr(11) <= cntrl2.btn_z;
	ledr(12) <= cntrl2.btn_r;
	ledr(13) <= cntrl2.btn_l;
	ledg <= "000000000";
	ledr(17 downto 14) <= "0000";

	ssd_cntrl_inst : ssd_cntrl
		PORT MAP(
			clk => sys_clk,
			res_n => sys_res_n,
			game_state => game_state,
			n64_cntrl => cntrl1,
			hex0 => hex0,
			hex1 => hex1,
			hex2 => hex2,
			hex3 => hex3,
			hex4 => hex4,
			hex5 => hex5,
			hex6 => hex6,
			hex7 => hex7
		);


	lcd_on <= '1';

	lcd_inst_not_dbg : if (not ENABLE_DISPLAY_DEBUG_INTERFACE) generate
		char_lcd_cntrl_inst : char_lcd_cntrl
			generic map (
				CLK_FREQ		=> SYS_CLK_FREQ
			)
			port map(
				clk				=> sys_clk,
				res_n			=> sys_res_n,
				instr_wr		=> char_lcd_instr_wr,
				instr			=> char_lcd_instr,
				instr_data		=> char_lcd_instr_data,
				instr_result	=> char_lcd_instr_result,
				busy			=> char_lcd_busy,
				lcd_data		=> lcd_data,
				lcd_en			=> lcd_en,
				lcd_rw			=> lcd_rw,
				lcd_rs			=> lcd_rs
			);

		dbg_char_lcd_clk			<= '0';
		dbg_char_lcd_res_n			<= '0';
		dbg_char_lcd_instr_result	<= (others => '0');
		dbg_char_lcd_busy			<= '0';
	end generate;


	lcd_inst_dbg : if ENABLE_DISPLAY_DEBUG_INTERFACE generate
		SIGNAL instr_wr			: STD_LOGIC;
		SIGNAL instr			: STD_LOGIC_VECTOR(3 DOWNTO 0);
		SIGNAL instr_data		: STD_LOGIC_VECTOR(15 DOWNTO 0);
		SIGNAL instr_result		: STD_LOGIC_VECTOR(15 DOWNTO 0);
		SIGNAL busy				: STD_LOGIC;
	begin



		display_cmd_mux_inst : display_cmd_mux
			port map (
				clk   				=> sys_clk,
				res_n 				=> sys_res_n,

				port_a_instr_wr     => dbg_char_lcd_instr_wr,
				port_a_instr        => dbg_char_lcd_instr,
				port_a_instr_data   => dbg_char_lcd_instr_data,
				port_a_instr_result => dbg_char_lcd_instr_result,
				port_a_busy         => dbg_char_lcd_busy,

				port_b_instr_wr     => char_lcd_instr_wr,
				port_b_instr        => char_lcd_instr,
				port_b_instr_data   => char_lcd_instr_data,
				port_b_instr_result => char_lcd_instr_result,
				port_b_busy         => char_lcd_busy,

				instr_wr     		=> instr_wr,
				instr        		=> instr,
				instr_data   		=> instr_data,
				instr_result 		=> instr_result,
				busy         		=> busy
			);

			char_lcd_cntrl_inst : char_lcd_cntrl
				generic map (
					CLK_FREQ		=> SYS_CLK_FREQ
				)
				port map(
					clk				=> sys_clk,
					res_n			=> sys_res_n,
					instr_wr		=> instr_wr,
					instr			=> instr,
					instr_data		=> instr_data,
					instr_result	=> instr_result,
					busy			=> busy,
					lcd_data		=> lcd_data,
					lcd_en			=> lcd_en,
					lcd_rw			=> lcd_rw,
					lcd_rs			=> lcd_rs
				);

			dbg_char_lcd_clk <= sys_clk;
			dbg_char_lcd_res_n <= sys_res_n;
	end generate;





END ARCHITECTURE;
