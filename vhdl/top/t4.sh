# moved to makefile!!!

VCOM_ARGS = -2008 -work work

SDO_FILE=./quartus/simulation/modelsim/top_7_1200mv_0c_vhd_slow.sdo
VHO_FILE=./quartus/simulation/modelsim/top_7_1200mv_0c_slow.vho

TB_FILE=./tb/top_tb.vhd
TB=top_tb
WAVE_FILE=./scripts/wave_t4.do

vlib work
vcom $VCOM_ARGS $VHO_FILE
vcom $VCOM_ARGS $TB_FILE
vsim -do "vsim $TB -sdftyp /uut=$SDO_FILE; do $WAVE_FILE; run -all"
#vsim -do "vsim $TB -sdftyp /uut=$SDO_FILE work.top_tb"
