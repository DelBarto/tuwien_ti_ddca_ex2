onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider clocks
add wave -noupdate /char_lcd_cntrl_tb/nclk
add wave -noupdate /char_lcd_cntrl_tb/res_n
add wave -noupdate -divider clocks
add wave -noupdate /char_lcd_cntrl_tb/instr
add wave -noupdate /char_lcd_cntrl_tb/instr_wr
add wave -noupdate /char_lcd_cntrl_tb/instr_data
add wave -noupdate /char_lcd_cntrl_tb/instr_result
add wave -noupdate -divider clocks
add wave -noupdate /char_lcd_cntrl_tb/lcd_data
add wave -noupdate /char_lcd_cntrl_tb/lcd_rs
add wave -noupdate /char_lcd_cntrl_tb/lcd_rw
add wave -noupdate /char_lcd_cntrl_tb/lcd_en
