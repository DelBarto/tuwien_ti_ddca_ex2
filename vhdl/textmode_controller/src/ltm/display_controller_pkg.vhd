
library ieee;
use ieee.std_logic_1164.all;
use work.math_pkg.all;
use work.font_pkg.all;


package display_controller_pkg is

	constant DISPLAY_WIDTH : integer := 800;
	constant DISPLAY_HEIGHT : integer := 480;
	
	constant COLOR_BLACK       : std_logic_vector(3 downto 0) := x"0";
	constant COLOR_DARK_RED    : std_logic_vector(3 downto 0) := x"1";
	constant COLOR_DARK_GREEN  : std_logic_vector(3 downto 0) := x"2";
	constant COLOR_DARK_YELLOW : std_logic_vector(3 downto 0) := x"3";
	constant COLOR_DARK_BLUE   : std_logic_vector(3 downto 0) := x"4";
	constant COLOR_DARK_PINK   : std_logic_vector(3 downto 0) := x"5";
	constant COLOR_DARK_CYAN   : std_logic_vector(3 downto 0) := x"6";
	constant COLOR_DARK_GRAY   : std_logic_vector(3 downto 0) := x"7";
	constant COLOR_GRAY        : std_logic_vector(3 downto 0) := x"8";
	constant COLOR_RED         : std_logic_vector(3 downto 0) := x"9";
	constant COLOR_GREEN       : std_logic_vector(3 downto 0) := x"a";
	constant COLOR_YELLOW      : std_logic_vector(3 downto 0) := x"b";
	constant COLOR_BLUE        : std_logic_vector(3 downto 0) := x"c";
	constant COLOR_PINK        : std_logic_vector(3 downto 0) := x"d";
	constant COLOR_CYAN        : std_logic_vector(3 downto 0) := x"e";
	constant COLOR_WHITE       : std_logic_vector(3 downto 0) := x"f";
	
	type COLOR_TYPE is array (0 to 15) of std_logic_vector(23 downto 0);
	
	constant COLOR_TABLE : COLOR_TYPE := (
		x"000000", -- black
		x"800000", -- dark red
		x"008000", -- dark green
		x"808000", -- dark yellow
		x"000080", -- dark blue
		x"800080", -- dark pink
		x"008080", -- dark cyan
		x"808080", -- dark gray
		x"C0C0C0", -- light gray
		x"ff0000", -- red
		x"00ff00", -- green
		x"ffff00", -- yellow
		x"0000ff", -- blue
		x"ff00ff", -- pink
		x"00ffff", -- cyan
		x"FFFFFF"  -- white
	);
--	constant COLOR_TABLE : COLOR_TYPE := (
--		x"000000", -- black
--		x"0000AA", -- blue
--		x"00AA00", -- green
--		x"00AAAA", -- cyan
--		x"AA0000", -- red
--		x"AA00AA", -- pink
--		x"AA5500", -- brown
--		x"AAAAAA", -- gray
--		x"555555", -- dark gray
--		x"5555FF", -- light blue
--		x"55FF55", -- light green
--		x"55FFFF", -- light cyan
--		x"FF5555", -- light red
--		x"FF55FF", -- 
--		x"FFFF55", -- yellow
--		x"FFFFFF"  -- white
--	);
	
	
	component display_controller is 
			generic (
			COLUMN_COUNT : integer := 100;
			ROW_COUNT : integer := 30
		);
		port (
			clk   : in  std_logic;   -- global system clk 
			res_n : in  std_logic;   -- system reset

			-- connection video ram
			vram_addr_row   : out std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
			vram_addr_colum : out std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
			vram_data       : in std_logic_vector(15 downto 0);
			vram_rd         : out std_logic;

			-- connection to font rom
			char              : out std_logic_vector(log2c(CHAR_COUNT)-1 downto 0);
			char_height_pixel : out std_logic_vector(log2c(CHAR_HEIGHT)-1 downto 0);
			decoded_char      : in std_logic_vector(0 to CHAR_WIDTH-1);


			vblank : out std_logic;

			-- connection to display
			hd   : out std_logic;         -- horizontal sync signal
			vd   : out std_logic;            -- vertical sync signal
			den  : out std_logic;            -- data enable 
			r    : out std_logic_vector(7 downto 0);  -- pixel color value (red)
			g    : out std_logic_vector(7 downto 0);  -- pixel color value (green)
			b    : out std_logic_vector(7 downto 0);  -- pixel color value (blue)

			grest : out std_logic -- display reset
		);
	end component;


end package;



