#!/bin/python3

import serial,time
import threading
from threading import Thread
import os
import signal
import sys
from docopt import docopt

import curses

n64_controller_image = r"""
              _.-------._
      _______|           |_______
   .-' [L]   |    [Z]    |   [R] '-.
 .'                                 '. 
/    [↑]                       [Λ]    \
| [←]   [→]                 [<]   [>] |
\    [↓]          [S]    [B]   [V]    /
|`.                        [A]      ,'|
|  `-.______     .---.     ______,-'  |
|      |    `.  | (-) |  ,'    |      |
|      |      \  '___'  /      |      |
|      /      |         |      \      |
\     /       | x[xxxx] |       \     /
 `._,'        \ y[yyyy] /        `._,' 
               \       /
                `.___,'
"""


class N64Controller:

	def ResetAllButtons(self):
		self.a = False
		self.b = False
		self.z = False
		self.start = False
		self.up = False
		self.down = False
		self.left = False
		self.right = False
		self.l = False
		self.r = False
		self.c_up = False
		self.c_down = False
		self.c_left = False
		self.c_right = False
		self.as_x = 0
		self.as_y = 0

	def __init__(self):
		self.a = False
		self.b = False
		self.z = False
		self.start = False
		self.up = False
		self.down = False
		self.left = False
		self.right = False
		self.l = False
		self.r = False
		self.c_up = False
		self.c_down = False
		self.c_left = False
		self.c_right = False
		self.as_x = 0
		self.as_y = 0
	
	def UpdateState(self, state):
		self.a =      bool(state[0] >> 0 & 1)
		self.b =      bool(state[0] >> 1 & 1)
		self.z =      bool(state[0] >> 2 & 1)
		self.start =  bool(state[0] >> 3 & 1)
		self.up =     bool(state[0] >> 4 & 1)
		self.down =   bool(state[0] >> 5 & 1)
		self.left =   bool(state[0] >> 6 & 1)
		self.right =  bool(state[0] >> 7 & 1)

		self.l =        bool(state[1] >> 2 & 1)
		self.r =        bool(state[1] >> 3 & 1)
		self.c_up =     bool(state[1] >> 4 & 1)
		self.c_down =   bool(state[1] >> 5 & 1)
		self.c_left =   bool(state[1] >> 6 & 1)
		self.c_right =  bool(state[1] >> 7 & 1)
		
		self.as_x = state[2]
		self.as_y = state[3]
	
	def GetState(self):
		byte_1  = (int(self.a) << 0) + (int(self.b) << 1) + (int(self.z) << 2) + (int(self.start) << 3) + (int(self.up) << 4) + (int(self.down) << 5) + (int(self.left) << 6) + (int(self.right) << 7) 
		byte_2 = (int(self.l) << 2 ) + (int(self.r) << 3 ) + (int(self.c_up) << 4 ) + (int(self.c_down) << 5 ) + (int(self.c_left) << 6 ) + (int(self.c_right) << 7)
		byte_3 = self.as_x & 0xff
		byte_4 = self.as_y & 0xff
		return bytes([byte_1, byte_2, byte_3, byte_4])
	
	def DrawASCIIArt(self):
		def sign_extend(value, bits):
			sign_bit = 1 << (bits - 1)
			return (value & (sign_bit - 1)) - (value & sign_bit)

		def coord_to_str(c):
			c_str = ""
			if (c > 0):
				c_str += "+"
			elif (c < 0):
				c_str += "-"
			else:
				c_str += " "
			
			c_str += str(abs(c)).rjust(3, " ")
			return c_str
		output = n64_controller_image
		r = [
		(self.a, "A"),
		(self.b, "B"), 
		(self.z, "Z"),
		(self.start, "S"),
		(self.up, "↑"),
		(self.down, "↓"),
		(self.up, "↑"),
		(self.left, "←"),
		(self.right, "→"),
		(self.l, "L"),
		(self.r, "R"),
		(self.c_up, "Λ"),
		(self.c_down, "V"),
		(self.c_left, "<"),
		(self.c_right, ">")
		 ]
		for x in r:
			if(not x[0]):
				output = output.replace("["+x[1]+"]", " " + x[1] + " ")
				
	
		as_x = sign_extend(self.as_x, 8)
		as_y = sign_extend(self.as_y, 8)

		output = output.replace("xxxx", coord_to_str(as_x))
		output = output.replace("yyyy", coord_to_str(as_y))
	
		return output

	def CopyStateFrom(self, other):
		self.a = other.a
		self.b = other.b
		self.z = other.z
		self.start = other.start
		self.up = other.up
		self.down = other.down
		self.left = other.left
		self.right = other.right
		self.l = other.l
		self.r = other.r
		self.c_up = other.c_up
		self.c_down = other.c_down
		self.c_left = other.c_left
		self.c_right = other.c_right
		self.as_x = other.as_x
		self.as_y = other.as_y

	def  __or__(self, other):
		temp_controller = N64Controller()
		temp_controller.a = other.a | self.a
		temp_controller.b = other.b | self.b
		temp_controller.z = other.z | self.z
		temp_controller.start = other.start | self.start
		temp_controller.up = other.up | self.up
		temp_controller.down = other.down | self.down
		temp_controller.left = other.left | self.left
		temp_controller.right = other.right | self.right
		temp_controller.l = other.l | self.l
		temp_controller.r = other.r | self.r
		temp_controller.c_up = other.c_up | self.c_up
		temp_controller.c_down = other.c_down | self.c_down
		temp_controller.c_left = other.c_left | self.c_left
		temp_controller.c_right = other.c_right | self.c_right
		temp_controller.as_x = other.as_x | self.as_x
		temp_controller.as_y = other.as_y | self.as_y
		return temp_controller


class UartThread(threading.Thread):
	def __init__(self, input_controller, output_controller, echo_mode, port, baud_rate):
		self.input_controller = input_controller
		self.output_controller = output_controller
		self.echo_mode = echo_mode
		self.stopevent = threading.Event()
		self.uart = serial.Serial()
		self.uart.port = port
		self.uart.baudrate = baud_rate
		self.uart.bytesize = serial.EIGHTBITS #number of bits per bytes
		self.uart.parity = serial.PARITY_NONE #set parity check: no parity
		self.uart.stopbits = serial.STOPBITS_ONE #number of stop bits
		self.uart.timeout = 1       # timeout in seconds
		self.uart.xonxoff = False
		self.uart.rtscts = False
		self.uart.dsrdtr = False
		threading.Thread.__init__(self)
		self.uart.open()


	def run(self):
		while(not self.stopevent.isSet()):
			read_data = self.uart.read(1)
			#print(hex(int(read_data[0])))
			if(len(read_data) == 0):
				break
			if(read_data == b"\xff"): # are we in sync
				state = self.uart.read(4)
				if(int(state[1]) & 0x03 != 0x03):
					#false sync
					self.uart.read(1)
					continue
				
				self.input_controller.UpdateState(state)
				if(self.echo_mode):
					self.output_controller.UpdateState(state)
				self.uart.write( b"\xff" + self.output_controller.GetState())
		
		self.uart.close()
	
	def join(self, timeout=None):
		self.stopevent.set()
		threading.Thread.join(self, timeout)


def signal_handler(sig, frame):
	uartThread.join()
	sys.exit(0)

__doc__ = """
Uart/N64 controller bridge control script

Usage: 
  controller.py [-b BAUD_RATE -d DEVICE -e]
  controller.py -h | -v 

Options:
  -h             Show this help
  -v --version   Show version information
  -d DEVICE      The UART device to use [default: /dev/ttyS0]
  -b BAUD_RATE   The baudrate of the UART interface [default: 9600]
  -e             Echo mode. The scripts simply echos the state it receives. No 
                 user input possible.
"""

uartThread = None
error_msg = None
arguments = None

n64_controller_keymap = {
	ord('a'): "a",
	ord('b'): "b",
	ord('z'): "z",
	ord('s'): "start",
	curses.KEY_UP : "up",
	curses.KEY_DOWN : "down",
	curses.KEY_LEFT : "left",
	curses.KEY_RIGHT : "right",
	ord('l'): "l",
	ord('r'): "r",
	ord('8'): "c_up",
	ord('5'): "c_down",
	ord('4'): "c_left",
	ord('6'): "c_right"
}

n64_controller_as_keymap = {
	ord('h'): ("as_x", -100),
	ord('k'): ("as_x", 100),
	ord('j'): ("as_y", -100),
	ord('u'): ("as_y", 100)
}


def curses_main(stdscr):
	global error_msg
	global uartThread
	global arguments
	
	echo_mode = arguments["-e"]
	port = arguments["-d"]
	baud_rate = arguments["-b"]
	
	input_controller = N64Controller()
	output_controller = N64Controller()
	state = [N64Controller(), N64Controller(),  N64Controller(), N64Controller()]
	signal.signal(signal.SIGINT, signal_handler)
	
	height, width = stdscr.getmaxyx()
	if(height < 20):
		error_msg = "Error: Increase the height of your terminal! (min. 20 lines)"
		return
	if(width < 80):
		error_msg = "Error: Increase the width of your terminal! (min. 80 columns)"
		return 
	try:
		uartThread = UartThread(input_controller, output_controller, echo_mode, port, baud_rate)
	except:
		error_msg = "Error: Unable to connect to the specified port (" + str(port) +")"
		return 

	left_win = stdscr.subwin(20, 40, 0, 0)
	right_win = stdscr.subwin(20, 40, 0, 40)
	
	uartThread.start()

	stdscr.clear()
	stdscr.refresh()
	stdscr.nodelay(True) # non-blocking getch

	stop = False
	while (not stop):
		curses.napms(75)
		
		if(not echo_mode):
			state[0].ResetAllButtons()
		
		while(True):
			c = stdscr.getch()
			if (c == curses.KEY_END or c == 27):
				stop = True
				break
			elif (c == -1):
				break
			
			if (not echo_mode):
				if (c in n64_controller_keymap.keys()):
					n = n64_controller_keymap[c]
					state[0].__dict__[n] = True
				if (c in n64_controller_as_keymap.keys()):
					n = n64_controller_as_keymap[c]
					state[0].__dict__[n[0]] = n[1]

		if(not echo_mode):
			new_state = N64Controller()
			new_state.ResetAllButtons()
			for x in state:
				new_state = new_state | x
			output_controller.CopyStateFrom(new_state)
			state.insert(0, state.pop()) # rotate 

		stdscr.clear()
		left_win.clear()
		right_win.clear()
		
		left_win.addstr("Input State:")
		left_win.addstr(input_controller.DrawASCIIArt())
		right_win.addstr("Output State: (echo mode: " + {True:"ON", False:"OFF"}[echo_mode] + ")" )
		right_win.addstr(output_controller.DrawASCIIArt())
		left_win.addstr("\nExit with Ctrl+C or End")
		right_win.refresh()
		left_win.refresh()
		stdscr.refresh()
		
		if(not uartThread.is_alive()):
			stop = True
			error_msg = "Error reading from uart port"
	uartThread.join(1)

if __name__ == "__main__":
	arguments = docopt(__doc__, version='1.0')
	curses.wrapper(curses_main)
	if(error_msg != None):
		print(error_msg)
		exit(1)



