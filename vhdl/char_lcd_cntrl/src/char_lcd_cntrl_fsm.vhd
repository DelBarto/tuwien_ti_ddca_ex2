
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.textmode_controller_pkg.all;
use work.display_controller_pkg.all;
use work.char_lcd_cntrl_pkg.all;
use work.math_pkg.all;

entity char_lcd_cntrl_fsm is
	generic (
		ROW_CNT : integer := 2;
		COLUMN_CNT : integer := 16;
		CLK_FREQ : integer := 25_000_000
	);
	port (
		clk				: in  std_logic;
		res_n			: in  std_logic;

		wr				: in std_logic;
		busy			: out std_logic;
		instr			: in std_logic_vector(3 downto 0);
		instr_data		: in std_logic_vector(15 downto 0);
		instr_result	: out std_logic_vector(15 downto 0);

		cntrl_data_in	: in std_logic_vector(7 downto 0);
		cntrl_data_out	: out std_logic_vector(7 downto 0);
		cntrl_en		: out std_logic;
		cntrl_rw		: out std_logic;
		cntrl_rs		: out std_logic;
		cntrl_busy		: in std_logic
	);
end entity;


architecture arch of char_lcd_cntrl_fsm is

	constant CHAR_LINEFEED : std_logic_vector(7 downto 0) := x"0A";
	constant CHAR_BACKSPACE : std_logic_vector(7 downto 0) := x"08";
	constant CHAR_CARRIAGE_RETURN : std_logic_vector(7 downto 0) := x"0D";

	type STATE_TYPE is (
		-- INITIALIZATION SEQUENCE
		POWER_ON, INIT_WAIT1, INIT_WAIT2, INIT_WAIT3,
		INIT1, INIT2, INIT3, INIT4, INIT_TURN_ON_DISPLAY,
		-- GENERAL STATES
		IDLE, CLEAR_SCREEN, SET_CHAR, DELETE, DELETE_NEXT, SET_CURSOR_POSITION,
		NEW_LINE, SET_CFG, MOVE_CURSOR_NEXT, NOP, GET_CHAR, SHIFT_CURSOR_LEFT,
		GET_CHAR_READ, WRITE_AC, MOVE_LINES, CURSOR_TO_NEW_LINE, CURSOR_TO_LAST_LINE,
		-- WAIT AFTER EVERY INSTRUCTION SEND TO THE LCD CONTROLLER
		WAIT_FOR_BUSY);

	signal return_state : STATE_TYPE;
	signal next_return_state : STATE_TYPE;
	signal lcd_cntrl_state : STATE_TYPE;
	signal next_lcd_cntrl_state : STATE_TYPE;
	signal clk_cnt : integer := 0;
	signal next_clk_cnt : integer := 0;

	signal instr_data_buffer : std_logic_vector(15 downto 0);
	signal instr_data_buffer_next : std_logic_vector(15 downto 0);

	signal instr_result_buffer : std_logic_vector(15 downto 0);
	signal instr_result_buffer_next : std_logic_vector(15 downto 0);


	-- column
	signal x_cursor : integer range 0 to COLUMN_CNT;
	signal x_cursor_next : integer range 0 to COLUMN_CNT;

	-- row
	signal y_cursor : integer range 0 to ROW_CNT;
	signal y_cursor_next : integer range 0 to ROW_CNT;

	-- configuration flags
	type cfg_register_t is
	record
		cursor_mode : std_logic_vector(1 downto 0);
		cursor_color : std_logic_vector(3 downto 0);
		auto_scroll : std_logic;
		auto_inc_cursor : std_logic;
		special_char_handling : std_logic;
	end record;
	signal cfg_register : cfg_register_t;
	signal cfg_register_next :cfg_register_t;

	-- scoll counter
	signal sig_scroll_offset : integer range 0 to ROW_CNT;
	signal sig_scroll_offset_next : integer range 0 to ROW_CNT;

	-- constants
	constant CLK_PERIODE_US : integer := CLK_FREQ/1_000_000;
	constant ONE_US : integer := 1_000_000/CLK_FREQ;
	constant ONE_NS : integer := 1_000_000_000/CLK_FREQ;


	type line_mem_t is array (0 to COLUMN_CNT-1) of std_logic_vector(7 downto 0);
	constant DEFAULT_LINE : line_mem_t := (others => x"20");
	type char_lcd_mem_t is array (0 to ROW_CNT-1) of line_mem_t;
	constant DEFAULT_CHAR_LCD_MEM : char_lcd_mem_t := (others => DEFAULT_LINE);

	signal char_lcd_mem			: char_lcd_mem_t := DEFAULT_CHAR_LCD_MEM;
	signal next_char_lcd_mem	: char_lcd_mem_t;
begin

-- -------------------------------------------------------
-- --					SYNC	PROCESS					--
-- -------------------------------------------------------
	sync : process(res_n, clk)
	begin

		if res_n = '0' then
			lcd_cntrl_state		<= POWER_ON;
			return_state		<= IDLE;
			x_cursor			<= 0;
			y_cursor			<= 0;
			sig_scroll_offset 	<= 0;

			cfg_register.cursor_mode 			<= CURSOR_STATE_BLINK;
			cfg_register.cursor_color 			<= "1010";
			cfg_register.auto_inc_cursor		<= '0';
			cfg_register.auto_scroll			<= '0';
			cfg_register.special_char_handling	<= '0';

			instr_data_buffer	<= (others=>'0');
			instr_result_buffer	<= (others=>'0');

			char_lcd_mem <= DEFAULT_CHAR_LCD_MEM;

		elsif rising_edge(clk) then
			return_state		<= next_return_state;
			lcd_cntrl_state		<= next_lcd_cntrl_state;
			clk_cnt				<= next_clk_cnt;

			instr_data_buffer	<= instr_data_buffer_next;
			x_cursor 			<= x_cursor_next;
			y_cursor 			<= y_cursor_next;
			sig_scroll_offset	<= sig_scroll_offset_next;

			cfg_register		<= cfg_register_next;
			instr_result_buffer	<= instr_result_buffer_next;

			char_lcd_mem 		<= next_char_lcd_mem;
		end if;

	end process sync;

-- -------------------------------------------------------
-- --				NEXT	STATE	LOGIC				--
-- -------------------------------------------------------
	next_state_logic : process (all)
	begin
		next_lcd_cntrl_state <= lcd_cntrl_state;
		next_clk_cnt <= clk_cnt;
		-- next_clk_cnt <= 0;
		next_return_state <= IDLE;

		case lcd_cntrl_state is
			when POWER_ON =>
			-- The execution time in the data sheet is given for a clock frequency  of 270Khz.
			-- If the frequency is different than the execution time changes and can be
			-- calculates as in (executionTime)*270Khz/(newClockFrequency)
				if ((clk_cnt*ONE_NS) > 20_000_000) then -- 15_000_000ns
					next_clk_cnt			<= 0;
					next_lcd_cntrl_state 	<= INIT_WAIT1;
				else
					next_clk_cnt 			<= clk_cnt + 1;
				end if;

			when INIT_WAIT1 =>
				if ((clk_cnt*ONE_NS) > 4_500_000) then -- 4_200_000ns
					next_clk_cnt 			<= 0;
					next_lcd_cntrl_state 	<= INIT_WAIT2;
				else
					next_clk_cnt <= clk_cnt + 1;
				end if;

			when INIT_WAIT2 =>
				if ((clk_cnt*ONE_NS) > 500_000) then -- 100_000ns
					next_clk_cnt 			<= 0;
					next_lcd_cntrl_state 	<= INIT_WAIT3;
				else
					next_clk_cnt <= clk_cnt + 1;
				end if;

			when INIT_WAIT3 =>
				if ((clk_cnt*ONE_NS) > 50_000) then -- 39_000ns
					next_clk_cnt 			<= 0;
					next_lcd_cntrl_state 	<= INIT1;
				else
					next_clk_cnt <= clk_cnt + 1;
				end if;

			when INIT1 =>	-- FUNTION SET
				if ((clk_cnt*ONE_NS) > 50_000) then -- 39_000ns
					next_clk_cnt <= 0;
					next_lcd_cntrl_state <= INIT2;
				else
					next_clk_cnt <= clk_cnt + 1;
				end if;

			when INIT2 =>	-- DISPLAY OFF
				if ((clk_cnt*ONE_NS) > 50_000) then -- 39_000ns
					next_clk_cnt <= 0;
					next_lcd_cntrl_state <= INIT3;
				else
					next_clk_cnt <= clk_cnt + 1;
				end if;

			when INIT3 =>	-- DISPLAY CLEAR
				if ((clk_cnt*ONE_NS) > 1_800_000) then -- 1_530_000ns
					next_clk_cnt <= 0;
					next_lcd_cntrl_state <= init4;
				else
					next_clk_cnt <= clk_cnt + 1;
				end if;

			when INIT4 =>	-- ENTRY MODE SET
				if ((clk_cnt*ONE_NS) > 50_000) then -- 39_000ns
					next_clk_cnt <= 0;
					next_lcd_cntrl_state <= INIT_TURN_ON_DISPLAY;
				else
					next_clk_cnt <= clk_cnt + 1;
				end if;

			when INIT_TURN_ON_DISPLAY =>
				if cntrl_busy = '0' then
					next_clk_cnt <= 0;
					next_return_state <= CLEAR_SCREEN;
					next_lcd_cntrl_state <= WAIT_FOR_BUSY;
				end if;

			when IDLE =>
				if wr = '1' then
					case instr is
						when INSTR_CLEAR_SCREEN =>  next_lcd_cntrl_state <= CLEAR_SCREEN;
						when INSTR_SET_CHAR =>
							next_lcd_cntrl_state <= SET_CHAR;
							if (cfg_register.special_char_handling = '1') then
								if (instr_data(7 downto 0) = CHAR_LINEFEED or
									instr_data(7 downto 0) = CHAR_CARRIAGE_RETURN) then --new line
									next_lcd_cntrl_state <= NEW_LINE;
								elsif instr_data(7 downto 0) = CHAR_BACKSPACE then
									next_lcd_cntrl_state <= DELETE;
								end if;
							end if;
						when INSTR_GET_CHAR =>
							if (cfg_register.auto_inc_cursor = '0') then
								next_return_state <= GET_CHAR;
								next_lcd_cntrl_state <= WRITE_AC;
							else
								next_lcd_cntrl_state <= GET_CHAR;
							end if;
						when INSTR_DELETE => next_lcd_cntrl_state <= DELETE;
						when INSTR_SET_CURSOR_POSITION => next_lcd_cntrl_state <= SET_CURSOR_POSITION;
						when INSTR_CFG => next_lcd_cntrl_state <= SET_CFG;
						when INSTR_MOVE_CURSOR_NEXT => next_lcd_cntrl_state <= MOVE_CURSOR_NEXT;
						when INSTR_NEW_LINE => next_lcd_cntrl_state <= NEW_LINE;
						when INSTR_NOP => next_lcd_cntrl_state <= NOP;
						when others =>
					end case;
				end if;

			when NOP =>
				if cntrl_busy = '0' then
					next_clk_cnt <= 0;
					next_lcd_cntrl_state <= WAIT_FOR_BUSY;
				end if;

			when CLEAR_SCREEN =>
				if cntrl_busy = '0' then
					next_clk_cnt <= 0;
					next_lcd_cntrl_state <= WAIT_FOR_BUSY;
				end if;

			when SET_CHAR =>
				if (cfg_register.auto_inc_cursor = '1') then
					if cntrl_busy = '0' then
						next_clk_cnt 			<= 0;
						next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
						if x_cursor = COLUMN_CNT-1 then
							next_return_state 	<= NEW_LINE;
						end if;
					end if;
				else
					if cntrl_busy = '0' then
						next_clk_cnt <= 0;
						next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
						next_return_state 		<= SHIFT_CURSOR_LEFT;
					end if;
				end if;

			when SHIFT_CURSOR_LEFT =>
				next_clk_cnt <= 0;
				next_lcd_cntrl_state <= WRITE_AC;

			when GET_CHAR =>
				if cntrl_busy = '0'  then
					next_clk_cnt			<= 0;
					next_lcd_cntrl_state	<= GET_CHAR_READ;
				end if;


			when GET_CHAR_READ =>
				if cntrl_busy ='0' then
					if (cfg_register.auto_inc_cursor = '0') then
						next_return_state		<= SHIFT_CURSOR_LEFT;
					end if;
					next_lcd_cntrl_state	<= WAIT_FOR_BUSY;
				end if;

			when DELETE =>
				next_return_state 		<= DELETE_NEXT;
				next_clk_cnt			<= 0;
				next_lcd_cntrl_state 	<= WRITE_AC;

			when DELETE_NEXT =>
				if cntrl_busy = '0' then
					next_clk_cnt			<= 0;
					next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
					next_return_state 		<= SHIFT_CURSOR_LEFT;
				end if;

			when SET_CURSOR_POSITION =>
				if cntrl_busy = '0' then
					next_clk_cnt			<= 0;
					next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
				end if;

			when MOVE_CURSOR_NEXT =>
				if x_cursor = COLUMN_CNT-1 then
					next_lcd_cntrl_state <= NEW_LINE;
				else
					if cntrl_busy = '0' then
						next_lcd_cntrl_state <= WAIT_FOR_BUSY;
					end if;
				end if;

			when NEW_LINE =>
				if (cfg_register.auto_scroll = '1') then
					if cntrl_busy = '0' then
						if y_cursor = ROW_CNT-1 then
							next_return_state		<= MOVE_LINES;
						end if;
						next_clk_cnt			<= 0;
						next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
					end if;
				else
					if cntrl_busy = '0' then
						next_clk_cnt			<= 0;
						next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
					end if;
				end if;

			when MOVE_LINES =>
				if cntrl_busy = '0' then
					next_clk_cnt 			<= 0;
					next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
					if x_cursor = COLUMN_CNT-1 and y_cursor = ROW_CNT-1 then
						next_return_state 	<= CURSOR_TO_LAST_LINE;
					else
						if x_cursor = COLUMN_CNT-1 then
							next_return_state 	<= CURSOR_TO_NEW_LINE;
						else
							next_return_state	<= MOVE_LINES;
						end if;
					end if;
				end if;

			when CURSOR_TO_NEW_LINE =>
				if cntrl_busy = '0' then
					next_clk_cnt 			<= 0;
					next_return_state		<= MOVE_LINES;
					next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
				end if;

			when CURSOR_TO_LAST_LINE =>
				if cntrl_busy = '0' then
					next_clk_cnt 			<= 0;
					next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
				end if;


			-- when CLEAR_LINE =>
			-- 	if cntrl_busy = '0' then
			-- 		next_return_state 		<= CLEAR_LINE_NEXT;
			-- 		next_clk_cnt			<= 0;
			-- 		next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
			-- 	end if;
			--
			-- when CLEAR_LINE_NEXT =>
			-- 	if x_cursor = COLUMN_CNT-1 then
			-- 		if cntrl_busy = '0' then
			-- 			next_clk_cnt			<= 0;
			-- 			next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
			-- 		end if;
			-- 	else
			-- 		if cntrl_busy = '0' then
			-- 			next_return_state 		<= CLEAR_LINE;
			-- 			next_clk_cnt			<= 0;
			-- 			next_lcd_cntrl_state 	<= WAIT_FOR_BUSY;
			-- 		end if;
			-- 	end if;

			when WRITE_AC =>
				next_return_state			<= return_state;
				if cntrl_busy = '0' then
					next_clk_cnt			<= 0;
					next_lcd_cntrl_state	<= WAIT_FOR_BUSY;
				end if;

			when SET_CFG =>
				if cntrl_busy = '0' then
					next_lcd_cntrl_state <= WAIT_FOR_BUSY;
				end if;

			when WAIT_FOR_BUSY		=> -- waits 2ms for
				next_return_state <= return_state;

				if ((clk_cnt*ONE_NS) >= 2_000_000) then
					next_clk_cnt <= 0;
					next_lcd_cntrl_state <= return_state;
				else
					next_clk_cnt <= clk_cnt + 1;
					next_lcd_cntrl_state <= lcd_cntrl_state;
				end if;



			when others =>
				null;
		end case;
	end process;


-- -------------------------------------------------------
-- --				OUTPUT	STATE	LOGIC				--
-- -------------------------------------------------------
	output_logic : process (all)
	begin
		busy			<= '1';
		cntrl_rw		<= '1';
		cntrl_en		<= '1';
		cntrl_rs		<= '0';
		cntrl_data_out	<= (others => '0');

		instr_data_buffer_next <= instr_data_buffer;

		x_cursor_next <= x_cursor;
		y_cursor_next <= y_cursor;

		next_char_lcd_mem <= char_lcd_mem;

		cfg_register_next <= cfg_register;
		sig_scroll_offset_next <= sig_scroll_offset;

		instr_result_buffer_next <= instr_result_buffer;
		instr_result <= instr_result_buffer;

		case lcd_cntrl_state is
			when POWER_ON =>
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				cntrl_data_out 	<= "00000000";

			when INIT_WAIT1 =>
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				cntrl_data_out 	<= "00111100";
				if clk_cnt = 0 then
					cntrl_en 		<= '0';
				end if;

			when INIT_WAIT2 =>
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				cntrl_data_out 	<= "00111100";
				if clk_cnt = 0 then
					cntrl_en 		<= '0';
				end if;

			when INIT_WAIT3 =>
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				cntrl_data_out 	<= "00111100";
				if clk_cnt = 0 then
					cntrl_en 		<= '0';
				end if;

			when INIT1 =>
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				cntrl_data_out 	<= "00111100";
				if clk_cnt = 0 then
					cntrl_en 		<= '0';
				end if;

			when INIT2 =>
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				cntrl_data_out 	<= "00001000";
				if clk_cnt = 0 then
					cntrl_en 		<= '0';
				end if;

			when INIT3 =>
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				cntrl_data_out	<= "00000001";
				if clk_cnt = 0 then
					cntrl_en 		<= '0';
				end if;

			when INIT4 =>
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				cntrl_data_out 	<= "00000110";	-- 000001(id)(S)
				if clk_cnt = 0 then
					cntrl_en 		<= '0';
				end if;

			when INIT_TURN_ON_DISPLAY =>
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				cntrl_data_out 	<= "00001111";	-- 000001(id)(S)
				if clk_cnt = 0 then
					cntrl_en 		<= '0';
				end if;

			when IDLE =>
				busy <= '0';
				if cntrl_busy = '1' then
					busy <= '1';
				end if;
				if wr = '1' then
					instr_data_buffer_next <= instr_data; --buffer input value
				end if;

			when CLEAR_SCREEN =>
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				cntrl_data_out 	<= "00000001";
				if cntrl_busy = '0' then
					cntrl_en 		<= '0';
				end if;

				x_cursor_next <= 0;
				y_cursor_next <= 0;
				next_char_lcd_mem <= DEFAULT_CHAR_LCD_MEM;
				-- if (x_cursor = COLUMN_CNT-1) then
				-- 	x_cursor_next <= 0;
				-- 	if (y_cursor = ROW_CNT-1) then
				-- 		y_cursor_next <= 0;
				-- 	else
				-- 		y_cursor_next <= y_cursor + 1;
				-- 	end if;
				-- else
				-- 	x_cursor_next <= x_cursor + 1;
				-- end if;

			when SET_CHAR =>
				cntrl_data_out 	<= instr_data_buffer(7 downto 0);
				next_char_lcd_mem(y_cursor)(x_cursor) <= instr_data_buffer(7 downto 0);
				cntrl_rs 		<= '1';
				cntrl_rw 		<= '0';
				if cntrl_busy = '0' then
					if x_cursor = COLUMN_CNT-1 then
						x_cursor_next <= 0;
					else
						x_cursor_next <= x_cursor + 1;
					end if;
					cntrl_en 	<= '0';
				end if;


			when SHIFT_CURSOR_LEFT =>
				if x_cursor = 0 then
					if y_cursor = 0 then
						x_cursor_next <= 0;
						y_cursor_next <= 0;
					else
						x_cursor_next <= COLUMN_CNT - 1;
						y_cursor_next <= y_cursor - 1;
					end if;
				else
					x_cursor_next <= x_cursor - 1;
					y_cursor_next <= y_cursor;
				end if;

			when GET_CHAR =>
				cntrl_rw <= '1';
				cntrl_rs <= '1';
				if cntrl_busy = '0' then
					cntrl_en 	<= '0';
				end if;


			when GET_CHAR_READ =>
				if cntrl_busy = '0' then
					x_cursor_next <= x_cursor + 1;
					instr_result_buffer_next 	<= COLOR_WHITE & COLOR_BLACK & cntrl_data_in;
					instr_result 				<= COLOR_WHITE & COLOR_BLACK & cntrl_data_in;
				end if;

			when DELETE => -- change x/y-cursor and then set it in WRITE_AC
				if x_cursor = 0 then
					if y_cursor = 0 then
						x_cursor_next <= 0;
						y_cursor_next <= 0;
					else
						x_cursor_next <= COLUMN_CNT - 1;
						y_cursor_next <= y_cursor - 1;
					end if;
				else
					x_cursor_next <= x_cursor - 1;
					y_cursor_next <= y_cursor;
				end if;

			when DELETE_NEXT =>
				cntrl_data_out 	<= x"20"; -- 0010 0000 - blank char
				next_char_lcd_mem(y_cursor)(x_cursor) <= x"20";
				cntrl_rw 		<= '0';
				cntrl_rs 		<= '1';
				if cntrl_busy = '0' then
					x_cursor_next <= x_cursor + 1;
					cntrl_en 		<= '0';
				end if;

			when SET_CURSOR_POSITION =>
				x_cursor_next <= to_integer(unsigned( instr_data_buffer(log2c(COLUMN_CNT)+8 downto 8) ));
				y_cursor_next <= to_integer(unsigned( instr_data_buffer(log2c(ROW_CNT) downto 0) ));
				cntrl_data_out 	<= '1' & std_logic_vector(to_unsigned(
											64*to_integer(unsigned( instr_data_buffer(log2c(ROW_CNT) downto 0) )) +
											to_integer(unsigned( instr_data_buffer(log2c(COLUMN_CNT)+8 downto 8) ))
										, 7));
				cntrl_rw 	<= '0';
				cntrl_rs 	<= '0';
				if cntrl_busy = '0' then
					cntrl_en 		<= '0';
				end if;

			when MOVE_CURSOR_NEXT =>
				if x_cursor = COLUMN_CNT-1 then
					x_cursor_next <= 0;
				else
					cntrl_data_out 	<= '1' & std_logic_vector(to_unsigned(64*y_cursor + (x_cursor+1),7));
					cntrl_rw 		<= '0';
					cntrl_rs 		<= '0';
					if cntrl_busy = '0' then
						x_cursor_next <= x_cursor + 1;
						cntrl_en 		<= '0';
					end if;
				end if;

			when NEW_LINE =>
				x_cursor_next <= 0;
				if (cfg_register.auto_scroll = '1') then --auto-scroll
				-- ???????????????????????????????????????
					if (y_cursor = ROW_CNT-1) then
						-- if (sig_scroll_offset = ROW_CNT-1) then
						-- 	sig_scroll_offset_next <= 0;
						-- else
						-- 	sig_scroll_offset_next <= sig_scroll_offset + 1;
						-- end if;

						-- SETTING CURSOR POSITION TO BEGINNING
						x_cursor_next <= 0;
						y_cursor_next <= 0;
						next_char_lcd_mem(0 to ROW_CNT-1) <= char_lcd_mem(1 to ROW_CNT-1) & DEFAULT_LINE;
						cntrl_data_out 	<= '1' & std_logic_vector(to_unsigned(64*0 + 0, 7));
						cntrl_rw 	<= '0';
						cntrl_rs 	<= '0';
						if cntrl_busy = '0' then
							cntrl_en 		<= '0';
						end if;

					else
						y_cursor_next <=  y_cursor + 1;
						cntrl_data_out 	<= '1' & std_logic_vector(to_unsigned(64*(y_cursor+1) + x_cursor, 7));
						cntrl_rw 	<= '0';
						cntrl_rs 	<= '0';
						if cntrl_busy = '0' then
							cntrl_en 		<= '0';
						end if;

					end if;
				-- ???????????????????????????????????????
				else -- no auto-scroll
					cntrl_rs	<= '0';
					cntrl_rw	<= '0';
					if (y_cursor = ROW_CNT-1) then
						cntrl_data_out	<= '1' & std_logic_vector(to_unsigned(64*0 + 0,7));
					else
						cntrl_data_out	<= '1' & std_logic_vector(to_unsigned(64*(y_cursor+1) + 0,7));
					end if;

					if cntrl_busy = '0' then
						if (y_cursor = ROW_CNT-1) then
							y_cursor_next	<= 0;
						else
							y_cursor_next	<= y_cursor + 1;
						end if;
						cntrl_en 		<= '0';
					end if;
				end if;


			when MOVE_LINES =>
				cntrl_data_out 	<= char_lcd_mem(y_cursor)(x_cursor);
				cntrl_rs 		<= '1';
				cntrl_rw 		<= '0';
				if cntrl_busy = '0' then
					if x_cursor = COLUMN_CNT-1 then
						x_cursor_next <= 0;
						if y_cursor < ROW_CNT-1 then
							y_cursor_next <= y_cursor + 1;
						end if;
					else
						x_cursor_next <= x_cursor + 1;
					end if;
					cntrl_en 	<= '0';
				end if;

			when CURSOR_TO_NEW_LINE =>
				x_cursor_next <= 0;
				y_cursor_next <= ROW_CNT-1;
				cntrl_data_out 	<= '1' & std_logic_vector(to_unsigned(64*y_cursor + x_cursor, 7));
				cntrl_rw 	<= '0';
				cntrl_rs 	<= '0';
				if cntrl_busy = '0' then
					cntrl_en 		<= '0';
				end if;

			when CURSOR_TO_LAST_LINE =>
				x_cursor_next <= 0;
				y_cursor_next <= ROW_CNT-1;
				cntrl_data_out 	<= '1' & std_logic_vector(to_unsigned(64*(ROW_CNT-1) + 0, 7));
				cntrl_rw 	<= '0';
				cntrl_rs 	<= '0';
				if cntrl_busy = '0' then
					cntrl_en 		<= '0';
				end if;

			-- when CLEAR_LINE_NEXT =>
			-- 	cntrl_data_out 	<= '1' & std_logic_vector(to_unsigned(64*ROW_CNT-1 + 0, 7));
			-- 	cntrl_rs 	<= '0';
			-- 	cntrl_rw 	<= '0';
			-- 	if cntrl_busy = '0' then
			-- 		cntrl_en 		<= '0';
			-- 	end if;

			-- when CLEAR_LINE =>
			-- 	cntrl_rs 		<= '0';
			-- 	cntrl_rw 		<= '0';
			-- 	cntrl_data_out 	<= '1' & std_logic_vector(to_unsigned(64*y_cursor + x_cursor,7));
			-- 	if cntrl_busy = '0' then
			-- 		cntrl_en 		<= '0';
			-- 	end if;
			--
			-- when CLEAR_LINE_NEXT =>
			-- 	cntrl_data_out 	<= x"20"; -- 0010 0000 - blank char
			-- 	next_char_lcd_mem(COLUMN_CNT*y_cursor + x_cursor) <= x"20";
			-- 	cntrl_rs 		<= '0';
			-- 	cntrl_rw 		<= '0';
			-- 	if cntrl_busy = '0' then
			-- 		cntrl_en 		<= '0';
			-- 	end if;
			--
			-- 	if (x_cursor = COLUMN_CNT-1) then
			-- 		x_cursor_next <= 0;
			-- 	else
			-- 		x_cursor_next <= x_cursor + 1;
			-- 	end if;

			when WRITE_AC =>
				cntrl_data_out 	<= '1' & std_logic_vector(to_unsigned(64*y_cursor + x_cursor,7));
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				if cntrl_busy = '0' then
					cntrl_en 		<= '0';
				end if;

			when SET_CFG =>
				-- CCCC***HISMM
				-- 111100011011 = F 1 B
				cfg_register_next.cursor_color <= instr_data_buffer(11 downto 8);	-- CCCC
				cfg_register_next.cursor_mode <= instr_data_buffer(1 downto 0);		-- MM
				cfg_register_next.auto_scroll <= instr_data_buffer(2);				-- S
				cfg_register_next.auto_inc_cursor <= instr_data_buffer(3);			-- I
				cfg_register_next.special_char_handling <= instr_data_buffer(4);	-- H

				cntrl_data_out 	<= x"0" & "11" & instr_data_buffer(0) & instr_data_buffer(1);
				cntrl_rs 		<= '0';
				cntrl_rw 		<= '0';
				if cntrl_busy = '0' then
					cntrl_en 		<= '0';
				end if;

			when WAIT_FOR_BUSY =>


			when others =>
		end case;
	end process;



end architecture;
