
library ieee;
use ieee.std_logic_1164.all;
use work.math_pkg.all;



package lcd_controller_pkg is

	constant COLOR_BLACK       : std_logic_vector(3 downto 0) := x"0";
	constant COLOR_WHITE       : std_logic_vector(3 downto 0) := x"f";

	component lcd_controller is
	generic (
		CLK_FREQ : integer := 25_000_000
	);
	port (
		clk				: in  std_logic;   -- global system clk
		res_n			: in  std_logic;   -- system reset

		cntrl_data_in	: in std_logic_vector(7 downto 0);
		cntrl_data_out	: out std_logic_vector(7 downto 0);
		cntrl_en		: in std_logic;
		cntrl_rw		: in std_logic;
		cntrl_rs		: in std_logic;
		cntrl_busy		: out std_logic;

		-- connection to display
		lcd_data		: inout std_logic_vector(7 downto 0);
		lcd_en			: out std_logic;
		lcd_rw			: out std_logic;
		lcd_rs			: out std_logic
	);
	end component;

end package;
