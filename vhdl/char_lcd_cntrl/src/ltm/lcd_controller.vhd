library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;
use ieee.numeric_std.all;
use work.math_pkg.all;
use work.lcd_controller_pkg.all;

entity lcd_controller is
	generic (
		CLK_FREQ : integer := 25_000_000
	);
	port (
		clk				: in	std_logic;   -- global system clk
		res_n			: in	std_logic;   -- system reset

		cntrl_data_in	: in 	std_logic_vector(7 downto 0);
		cntrl_data_out	: out 	std_logic_vector(7 downto 0);
		cntrl_en		: in 	std_logic;
		cntrl_rw		: in 	std_logic;
		cntrl_rs		: in 	std_logic;
		cntrl_busy		: out 	std_logic;

		-- connection to display
		lcd_data		: inout	std_logic_vector(7 downto 0);
		lcd_en			: out	std_logic;
		lcd_rw			: out	std_logic;
		lcd_rs			: out	std_logic
	);
end entity;


architecture beh of lcd_controller is

	type LCD_CONTROLLER_STATE is (IDLE, SET_RSRW, SET_E, RDATA, WDATA,
									UNSET_E_W, UNSET_E_R);

	signal state 				: LCD_CONTROLLER_STATE := IDLE;
	signal next_state 			: LCD_CONTROLLER_STATE := IDLE;

	signal clk_cnt 				: integer := 0;
	signal next_clk_cnt			: integer := 0;

	signal buf_data_in			: std_logic_vector(7 downto 0);
	signal buf_data_out			: std_logic_vector(7 downto 0);
	signal buf_rw				: std_logic := '0';
	signal buf_rs				: std_logic := '0';
	signal next_buf_data_in		: std_logic_vector(7 downto 0);
	signal next_buf_data_out	: std_logic_vector(7 downto 0);
	signal next_buf_rw			: std_logic;
	signal next_buf_rs			: std_logic;


	constant ONE_NS : integer := 1_000_000_000/CLK_FREQ;


	signal buf_lcd_data			: std_logic_vector(7 downto 0);
	signal next_buf_lcd_data	: std_logic_vector(7 downto 0);

begin


-- -------------------------------------------------------
-- --					SYNC	PROCESS					--
-- -------------------------------------------------------
	sync : process(clk, res_n)
	begin
		if res_n = '0' then -- reset
			-- reset counter signals
			clk_cnt 		<= 0;
			state			<= IDLE;
			buf_data_out	<= "00000000";
			buf_data_in		<= "00000000";
			buf_rw			<= '0';
			buf_rs			<= '0';


			buf_lcd_data	<= (others => '0');
		elsif rising_edge(clk) then
			clk_cnt 		<= next_clk_cnt;
			state 			<= next_state;
			buf_data_out	<= next_buf_data_out;
			buf_data_in		<= next_buf_data_in;
			buf_rw			<= next_buf_rw;
			buf_rs			<= next_buf_rs;


			buf_lcd_data	<= next_buf_lcd_data;
		end if;
	end process sync;

	-- -------------------------------------------------------
	-- --				NEXT	STATE	LOGIC				--
	-- -------------------------------------------------------
		logic_next_state : process(all)
		begin
			next_state <= state;
			next_clk_cnt <= clk_cnt;
			next_buf_data_out <= buf_data_out;
			next_buf_data_in <= buf_data_in;
			next_buf_rs <= buf_rs;
			next_buf_rw <= buf_rw;


			next_buf_lcd_data	<= buf_lcd_data;

			case( state ) is
				when IDLE 			=>
					if (cntrl_en = '0') then
						next_buf_data_in	<= cntrl_data_in;
						next_buf_rw			<= cntrl_rw;
						next_buf_rs			<= cntrl_rs;
						next_clk_cnt 		<= 0;
						next_state 			<= SET_RSRW;
					end if;

				when SET_RSRW 		=>
					if ((clk_cnt*ONE_NS) >= 40) then
						next_clk_cnt 	<= 0;
						next_state 		<= SET_E;
						next_buf_lcd_data <= buf_data_in;
					else
						next_clk_cnt 	<= clk_cnt + 1;
					end if;

				when SET_E			=>
					if (buf_rw = '1') then
						if ((clk_cnt*ONE_NS) >= 160) then
							next_clk_cnt	<= 0;
							next_state		<= RDATA;
						else
							next_clk_cnt 	<= clk_cnt + 1;
						end if;
					else
						next_clk_cnt <= 0;
						next_state <= WDATA;
					end if;

				when RDATA			=>
					if ((clk_cnt*ONE_NS) >= 70) then
						next_buf_data_out <= lcd_data;
						next_clk_cnt <= 0;
						next_state <= UNSET_E_R;
					else
						next_clk_cnt <= clk_cnt + 1;
					end if;

				when WDATA			=>
					if ((clk_cnt*ONE_NS) >= 200) then -- 230 - 40 ns due to one clk cyc in SET_E
						next_clk_cnt <= 0;
						next_state <= UNSET_E_W;
					else
						next_clk_cnt <= clk_cnt + 1;
					end if;

				when UNSET_E_W	=>
					if ((clk_cnt*ONE_NS) >= 40) then
						next_clk_cnt <= 0;
						next_state <= IDLE;
					else
						next_clk_cnt <= clk_cnt + 1;
					end if;

				when UNSET_E_R	=>
					if ((clk_cnt*ONE_NS) >= 70) then
						next_clk_cnt <= 0;
						next_state <= IDLE;
					else
						next_clk_cnt <= clk_cnt + 1;
					end if;

			end case;

		end process logic_next_state;





-- -------------------------------------------------------
-- --				OUTPUT	STATE	LOGIC				--
-- -------------------------------------------------------
	output : process(all)
	begin
		cntrl_busy	<= '1';
		lcd_rs		<= buf_rs;
		lcd_rw		<= buf_rw;
		lcd_data	<= buf_lcd_data;
		lcd_en		<= '0';

		cntrl_data_out <= buf_data_out;

		case( state ) is
			when IDLE 			=>
				cntrl_busy <= '0';

			when SET_RSRW 		=>
				lcd_rs <= buf_rs;
				lcd_rw <= buf_rw;

			when SET_E			=>
				lcd_rs <= buf_rs;
				lcd_rw <= buf_rw;
				if (buf_rs = '1' and buf_rw = '1') then--
					lcd_data	<= (others => 'Z');--
				end if;--
				lcd_en <= '1';

			when RDATA			=>
				lcd_rs <= buf_rs;
				lcd_rw <= buf_rw;
				lcd_en <= '1';
				lcd_data	<= (others => 'Z');--

			when WDATA			=>
				lcd_rs <= buf_rs;
				lcd_rw <= buf_rw;
				lcd_en <= '1';
				lcd_data <= buf_data_in;

			when UNSET_E_R	=>
				lcd_rs <= buf_rs;
				lcd_rw <= buf_rw;
				lcd_data	<= (others => 'Z');--

			when UNSET_E_W	=>
				lcd_rs <= buf_rs;
				lcd_rw <= buf_rw;
				lcd_data <= buf_data_in;

		end case;
	end process output;


end architecture;
