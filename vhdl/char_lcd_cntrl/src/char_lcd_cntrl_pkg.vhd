library ieee;
use ieee.std_logic_1164.all;
use work.lcd_controller_pkg.all;
use work.textmode_controller_pkg.all;
use work.display_controller_pkg.all;


package char_lcd_cntrl_pkg is

	constant ROW_CNT : integer := 2;
	constant COLUMN_CNT : integer := 16;

	-- constant COLOR_BLACK       : std_logic_vector(3 downto 0) := x"0";
	-- constant COLOR_WHITE       : std_logic_vector(3 downto 0) := x"f";

	-- cursor options
	-- constant CURSOR_STATE_OFF : std_logic_vector(1 downto 0) := "00";
	-- constant CURSOR_STATE_ON : std_logic_vector(1 downto 0) := "01";
	-- constant CURSOR_STATE_BLINK : std_logic_vector(1 downto 0) := "11";

	subtype instr_t is std_logic_vector(3 downto 0);

	--Instructions for this textmode video controller
	-- constant INSTR_NOP 					: instr_t := (others=>'0');
	-- constant INSTR_SET_CHAR 			: instr_t := x"1";
	-- constant INSTR_CLEAR_SCREEN 		: instr_t := x"2";
	-- constant INSTR_SET_CURSOR_POSITION 	: instr_t := x"3";
	-- constant INSTR_CFG 					: instr_t := x"4";
	-- constant INSTR_DELETE 				: instr_t := x"5";
	-- constant INSTR_MOVE_CURSOR_NEXT 	: instr_t := x"6";
	-- constant INSTR_NEW_LINE 			: instr_t := x"7";
	-- constant INSTR_GET_CHAR 			: instr_t := x"8";

	component char_lcd_cntrl is
		generic (
			CLK_FREQ : integer := 25_000_000
		);
		port (
			clk          : in std_logic;
			res_n        : in std_logic;
			instr_wr     : in std_logic;
			instr        : in std_logic_vector(3 downto 0);
			instr_data   : in std_logic_vector(15 downto 0);
			instr_result : out std_logic_vector(15 downto 0);
			busy         : out std_logic;
			lcd_data     : inout std_logic_vector(7 downto 0);
			lcd_en       : out std_logic;
			lcd_rw       : out std_logic;
			lcd_rs       : out std_logic
		);
	end component;
end package;
