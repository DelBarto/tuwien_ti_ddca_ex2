library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.char_lcd_cntrl_pkg.all;
use work.lcd_controller_pkg.all;


entity char_lcd_cntrl is
	generic (
		CLK_FREQ		: integer := 25_000_000
	);
	port (
		clk				: in  std_logic;
		res_n			: in  std_logic;

		instr_wr		: in std_logic;
		instr			: in std_logic_vector(3 downto 0);
		instr_data		: in std_logic_vector(15 downto 0);
		instr_result	: out std_logic_vector(15 downto 0);
		busy			: out std_logic;

		lcd_data		: inout std_logic_vector(7 downto 0);
		lcd_en			: out std_logic;
		lcd_rw			: out std_logic;
		lcd_rs			: out std_logic
	);
end entity;

--Exercise 2: Task 1
architecture beh of char_lcd_cntrl is
	component char_lcd_cntrl_fsm is
		generic (
			ROW_CNT 		: integer;
			COLUMN_CNT 	: integer;
			CLK_FREQ 		: integer
		);
		port (
			clk				: in  std_logic;
			res_n			: in  std_logic;

			wr				: in std_logic;
			busy			: out std_logic;
			instr			: in std_logic_vector(3 downto 0);
			instr_data		: in std_logic_vector(15 downto 0);
			instr_result	: out std_logic_vector(15 downto 0);

			cntrl_data_in	: in std_logic_vector(7 downto 0);
			cntrl_data_out	: out std_logic_vector(7 downto 0);
			cntrl_en		: out std_logic;
			cntrl_rw		: out std_logic;
			cntrl_rs		: out std_logic;
			cntrl_busy		: in std_logic
		);
	end component;

	signal cntrl_data_in	: std_logic_vector(7 downto 0);
	signal cntrl_data_out	: std_logic_vector(7 downto 0);
	signal cntrl_en			: std_logic;
	signal cntrl_rw			: std_logic;
	signal cntrl_rs			: std_logic;
	signal cntrl_busy		: std_logic;

begin

	controller_inst : char_lcd_cntrl_fsm
		generic map(
			ROW_CNT 		=>  ROW_CNT,
			COLUMN_CNT		=> COLUMN_CNT,
			CLK_FREQ 		=> CLK_FREQ
		)
		port map(
			clk				=> clk,
			res_n			=> res_n,

			wr				=> instr_wr,
			busy			=> busy,
			instr			=> instr,
			instr_data		=> instr_data,
			instr_result	=> instr_result,

			cntrl_data_in	=> cntrl_data_in,
			cntrl_data_out	=> cntrl_data_out,
			cntrl_en		=> cntrl_en,
			cntrl_rw		=> cntrl_rw,
			cntrl_rs		=> cntrl_rs,
			cntrl_busy		=> cntrl_busy
		);

	lcd_controller_inst : lcd_controller
		generic map(
			CLK_FREQ 		=> CLK_FREQ
		)
		port map(
			clk				=> clk,
			res_n			=> res_n,

			cntrl_data_in	=> cntrl_data_out,
			cntrl_data_out	=> cntrl_data_in,
			cntrl_en		=> cntrl_en,
			cntrl_rw		=> cntrl_rw,
			cntrl_rs		=> cntrl_rs,
			cntrl_busy		=> cntrl_busy,

			lcd_data		=> lcd_data,
			lcd_en			=> lcd_en,
			lcd_rw			=> lcd_rw,
			lcd_rs			=> lcd_rs
		);


end architecture;
