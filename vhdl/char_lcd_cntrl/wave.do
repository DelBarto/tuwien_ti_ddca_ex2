onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider generic
add wave -noupdate /char_lcd_cntrl_tb/clk
add wave -noupdate /char_lcd_cntrl_tb/res_n
add wave -noupdate /char_lcd_cntrl_tb/busy
add wave -noupdate -divider input
add wave -noupdate /char_lcd_cntrl_tb/instr
add wave -noupdate /char_lcd_cntrl_tb/instr_wr
add wave -noupdate /char_lcd_cntrl_tb/instr_data
add wave -noupdate /char_lcd_cntrl_tb/instr_result
add wave -noupdate -divider output
add wave -noupdate /char_lcd_cntrl_tb/lcd_data
add wave -noupdate /char_lcd_cntrl_tb/lcd_rs
add wave -noupdate /char_lcd_cntrl_tb/lcd_rw
add wave -noupdate /char_lcd_cntrl_tb/lcd_en
add wave -noupdate -divider lcd_controller
add wave -noupdate /char_lcd_cntrl_tb/uut/lcd_controller_inst/state
add wave -noupdate /char_lcd_cntrl_tb/uut/lcd_controller_inst/clk_cnt
add wave -noupdate /char_lcd_cntrl_tb/uut/lcd_controller_inst/buf_data_out
add wave -noupdate -divider fsm
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/wr
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/lcd_cntrl_state
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/return_state
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/x_cursor
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/y_cursor
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/cntrl_en
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/cntrl_rw
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/cntrl_rs
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/cntrl_busy
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/cntrl_data_in
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/cntrl_data_out
add wave -noupdate -divider fsm_mem
add wave -noupdate /char_lcd_cntrl_tb/uut/controller_inst/char_lcd_mem
