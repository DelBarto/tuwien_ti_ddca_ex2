-- TASK 5: put your code here

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;
use work.char_lcd_cntrl_pkg.all;
use work.textmode_controller_pkg.all;

entity char_lcd_cntrl_tb is
end entity;

architecture bench of char_lcd_cntrl_tb is

	component char_lcd_cntrl is
		generic (
			CLK_FREQ 		: integer := 25_000_000
		);
		port (
			clk				: in std_logic;
			res_n			: in std_logic;
			instr_wr		: in std_logic;
			instr			: in std_logic_vector(3 downto 0);
			instr_data		: in std_logic_vector(15 downto 0);
			instr_result	: out std_logic_vector(15 downto 0);
			busy			: out std_logic;
			lcd_data		: inout std_logic_vector(7 downto 0);
			lcd_en			: out std_logic;
			lcd_rw			: out std_logic;
			lcd_rs			: out std_logic
		);
	end component;

	signal clk				: std_logic;
	signal res_n			: std_logic;
	signal instr_wr			: std_logic;
	signal instr			: std_logic_vector(3 downto 0);
	signal instr_data		: std_logic_vector(15 downto 0);
	signal instr_result		: std_logic_vector(15 downto 0);
	signal busy				: std_logic;
	signal lcd_data			: std_logic_vector(7 downto 0);
	signal lcd_en			: std_logic;
	signal lcd_rw			: std_logic;
	signal lcd_rs			: std_logic;


	constant CLK_FREQ : integer := 25_000_000;
	constant TIMING_SCALAR : integer := integer(ceil(real(270_000/25_000_000)));
	constant CLK_PERIOD : time := 40 ns; -- 1 000 000 000 / 25 000 000
	signal stop_clock : boolean := false;

	constant c : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(32 + 1526618 mod 94,8));
begin
	uut : char_lcd_cntrl
	port map(
		clk				=> clk,
		res_n			=> res_n,
		instr_wr		=> instr_wr,
		instr			=> instr,
		instr_data		=> instr_data,
		instr_result	=> instr_result,
		busy			=> busy,
		lcd_data		=> lcd_data,
		lcd_en			=> lcd_en,
		lcd_rw			=> lcd_rw,
		lcd_rs			=> lcd_rs
	);


	stimulus : process
	begin

		report "-----START------";
		-- initialisation


		res_n <= '0';
		instr_wr <= '0';
		instr <= (others => '0');
		instr_data <= (others => '0');
		wait for CLK_PERIOD;
		res_n <= '1';

		-- (20000+2*5000+40*2+1530)=31610 us
		-- wait for 346 us;
		wait for 33 ms;
		-- report "------> waited for " & to_string(real(32_000.0*270_000.0/CLK_FREQ)) & " us";

		wait for CLK_PERIOD;
		instr <= INSTR_CFG;
		instr_data <= (others => '0');
		instr_wr <= '1';
		wait for CLK_PERIOD;
		instr_wr <= '0';
		wait for CLK_PERIOD;

		if not busy = '0' then
			wait until busy = '0';
		end if;
		instr <= INSTR_SET_CURSOR_POSITION;
		instr_data <= x"0201";
		instr_wr <= '1';
		wait for CLK_PERIOD;
		instr_wr <= '0';
		wait for CLK_PERIOD;

		if not busy = '0' then
			wait until busy = '0';
		end if;
		instr <= INSTR_SET_CHAR;
		instr_data <= x"00" & c;
		instr_wr <= '1';
		wait for CLK_PERIOD;
		instr_wr <= '0';
		wait for CLK_PERIOD;

		if not (busy = '0') then
			wait until busy = '0';
		end if;
		instr <= INSTR_GET_CHAR;
		instr_data <= (others => '0');
		instr_wr <= '1';
		wait for CLK_PERIOD;
		instr_wr <= '0';
		wait for CLK_PERIOD;

		if not (busy = '0') then
			wait until busy = '0';
		end if;

		report "instr_result(7 downto 0) = " & to_string(instr_result(7 downto 0));
		report "c = " & to_string(c);
		assert instr_result(7 downto 0) = c;


		wait for 2_000*CLK_PERIOD;


		-- auto_scroll test
		instr <= INSTR_CFG;
		instr_data <= x"0F1F";
		instr_wr <= '1';
		wait for CLK_PERIOD;
		instr_wr <= '0';
		wait for CLK_PERIOD;

		if not busy = '0' then
			wait until busy = '0';
		end if;
		instr <= INSTR_SET_CURSOR_POSITION;
		instr_data <= x"0F01";
		instr_wr <= '1';
		wait for CLK_PERIOD;
		instr_wr <= '0';
		wait for CLK_PERIOD;

		if not busy = '0' then
			wait until busy = '0';
		end if;
		instr <= INSTR_SET_CHAR;
		instr_data <= x"00" & x"32";
		instr_wr <= '1';
		wait for CLK_PERIOD;
		instr_wr <= '0';
		wait for CLK_PERIOD;


		wait for 2_000_000*CLK_PERIOD;


		report "-----END-----";
		stop_clock <= true;
		wait;
	end process;

	busy_flag_or_return_char : process(clk)
	begin
		if (rising_edge(clk)) then
			if (lcd_rs = '1' and lcd_rw = '1' and lcd_en = '1') then
				lcd_data <= c;
			else
				lcd_data <= (others => 'Z');
			end if;
		end if;
	end process;


	generate_clk : process
	begin
		while not stop_clock loop
		clk <= '0', '1' after CLK_PERIOD / 2;
		wait for CLK_PERIOD;
	end loop;
	wait;
	end process;
end architecture;
