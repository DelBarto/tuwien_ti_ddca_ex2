library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.snake_pkg.all;
use work.textmode_controller_pkg.all;
use work.display_controller_pkg.all;
use work.math_pkg.all;
use work.n64_controller_pkg.all;
use work.audio_cntrl_pkg.all;
use work.snake_pkg.all;
use work.snake_game_pkg.all;
use work.prng_pkg.all;
use work.lcd_writer_pkg.all;
use work.food_placer_pkg.all;
use work.sound_maker_pkg.all;
-- use work.char_lcd_cntrl_pkg.all;

entity snake_game is
	port (
		clk : in std_logic;
		res_n : in std_logic;

		--connection to the textmode controller
		instr_wr     : out std_logic;
		instr        : out std_logic_vector(3 downto 0);
		instr_data   : out std_logic_vector(15 downto 0);
		instr_result : in std_logic_vector(15 downto 0);
		busy         : in std_logic;
		vblank       : in std_logic;

		--connections to the N64 controllers
		cntrl1 : in n64_buttons_t;
		cntrl2 : in n64_buttons_t;

		--connection to the audio controller
		synth_cntrl : out synth_cntrl_vec_t(0 to 1);

		--connection to the character LCD controller
		char_lcd_instr_wr     : out std_logic;
		char_lcd_instr        : out std_logic_vector(3 downto 0);
		char_lcd_instr_data   : out std_logic_vector(15 downto 0);
		char_lcd_instr_result : in std_logic_vector(15 downto 0);
		char_lcd_busy         : in std_logic;

		--misc
		game_state : out snake_game_state_t
	);
end entity;

architecture arch of snake_game is

	type state_t is (
		-- initial states
		RESET, RESET_GAME, INIT_SET_CFG, INIT_CLEAR_DISPLAY, INIT_WAIT_CLEAR,
		INIT_FOOD, PLACE_SNAKE, PLACE_SNAKE_WAIT,
		INIT_WAIT_CLEAR_FOOD_PLACER, INIT_PLACING_FOOD, INIT_WAIT_FOOD_PLACER,
		-- game mechanic
		WAIT_VBLANK, PROCESS_INPUTS, PAUSING, DEAD,
		-- checking if snake hits something
		CURSOR_SET, CURSOR_REQUEST_CHAR, CURSOR_CHECK,
		-- for placing the food
		DELETE_SPECIAL_FOOD, WAIT_FOOD_PLACER_DELETE,
		PLACING_FOOD, PLACING_SPECIAL_FOOD, WAIT_FOOD_PLACER,
		-- move snake
		MOVE_HEAD, WAIT_MOVE_HEAD, MOVE_TAIL, WAIT_MOVE_TAIL
	);

	signal state : state_t;
	signal next_state : state_t;

	signal frame_counter : std_logic_vector(log2c(FRAMES_BETWEEN_MOVEMENTS)-1 downto 0);
	signal frame_counter_next : std_logic_vector(frame_counter'range);

	signal prev_vblank : std_logic;


	--wires to snake initalizer
	signal snake_instr_wr : std_logic;
	signal snake_instr : std_logic_vector(3 downto 0);
	signal snake_instr_data : std_logic_vector(15 downto 0);
	signal snake_instr_result : std_logic_vector(15 downto 0);
	signal snake_done : std_logic;

	signal snake_init : std_logic;
	signal snake_move_head : std_logic;
	signal snake_move_tail : std_logic;

	signal movement_direction : direction_t;
	signal movement_direction_next : direction_t;

	signal rnd_vec : std_logic_vector(1 downto 0);
	signal skip_move : std_logic;
	signal skip_move_next : std_logic;

	signal load_seed : std_logic;

	-- added
	signal buf_game_state : snake_game_state_t;
	signal next_buf_game_state : snake_game_state_t;
	signal is_dead : boolean;
	signal next_is_dead : boolean;
	signal highscore : integer := 0;
	signal next_highscore : integer;
	signal score : integer := 0;
	signal next_score : integer;

	signal tail_movement : std_logic;
	signal next_tail_movement : std_logic;


	signal sf_cntdwn : integer := 0;
	signal next_sf_cntdwn : integer;
	constant SPECIAL_FOOD_TIMEOUT_CLK_TICKS : integer := SPECIAL_FOOD_TIMEOUT_MS * (10**6/ONE_NS);

	signal sf_threshold_cnt : integer := 0;
	signal next_sf_threshold_cnt : integer := 0;

	signal snake_head_position : vec2d_t;


	signal fp_wr				: std_logic;
	signal fp_sd_food			: std_logic;
	signal fp_food_type			: std_logic;
	signal fp_busy				: std_logic;
	signal fp_done				: std_logic;
	signal fp_instr_wr			: std_logic;
	signal fp_instr				: std_logic_vector(3 downto 0);
	signal fp_instr_data		: std_logic_vector(15 downto 0);



begin

	game_state <= buf_game_state;

	sync : process(clk, res_n)
	begin
		if (res_n = '0') then
			state				<= RESET;
			frame_counter		<= (others=>'0');
			prev_vblank			<= '0';
			movement_direction	<= UP;
			skip_move			<= '0';
			is_dead				<= false;

			buf_game_state		<= IDLE;
			highscore			<= 0;
			score				<= 0;
			sf_cntdwn			<= 0;
			sf_threshold_cnt	<= 0;
			tail_movement		<=  '1';

		elsif (rising_edge(clk)) then
			state				<= next_state;
			frame_counter		<= frame_counter_next;
			prev_vblank			<= vblank;
			movement_direction	<= movement_direction_next;
			skip_move			<= skip_move_next;
			is_dead				<= next_is_dead;

			buf_game_state		<= next_buf_game_state;
			highscore			<= next_highscore;
			score				<= next_score;
			sf_cntdwn			<= next_sf_cntdwn;
			sf_threshold_cnt	<= next_sf_threshold_cnt;
			tail_movement		<= next_tail_movement;
		end if;
	end process;



	next_state_logic : process(all)
		variable tmp_dir : direction_t;
	begin
		tmp_dir := movement_direction;
		skip_move_next <= skip_move;
		load_seed <= '0';


		instr_wr   <= '0';
		instr      <= (others=>'0');
		instr_data <= (others=>'0');

		snake_move_head <= '0';
		snake_move_tail <= '0';
		snake_init <= '0';

		fp_wr			<= '0';
		fp_sd_food		<= '0';
		fp_food_type	<= '0';

		next_state <= state;
		frame_counter_next <= frame_counter;

		movement_direction_next <= movement_direction;

		-- added
		next_highscore			<= highscore;
		next_score				<= score;
		next_is_dead			<= is_dead;


		next_sf_cntdwn <= sf_cntdwn;
		if (sf_cntdwn > 1) and buf_game_state = RUNNING then
			next_sf_cntdwn <= sf_cntdwn - 1;
		end if;

		next_sf_threshold_cnt <= sf_threshold_cnt;

		next_tail_movement <= tail_movement;

		case state is

			when RESET =>
				load_seed <= '1';
				if(busy = '0') then
					next_state <= INIT_SET_CFG;
				end if;

			when INIT_SET_CFG =>
				instr_wr <= '1';
				instr <= INSTR_CFG;
				-- instr_data <= x"0" & COLOR_BLUE & x"0" & '1' & '0' & CURSOR_STATE_BLINK;
				instr_data <= x"0" & COLOR_BLUE & x"0" & '0' & '0' & CURSOR_STATE_OFF;
				next_state <= INIT_CLEAR_DISPLAY;

			when RESET_GAME =>
				next_is_dead <= false;
				next_score <= 0;
				next_sf_cntdwn <= 0;
				next_sf_threshold_cnt <= 0;
				if (busy = '0') then
					instr_wr <= '1';
					instr <= INSTR_SET_CURSOR_POSITION;
					instr_data <= (others => '0');
					next_state <= INIT_CLEAR_DISPLAY;
				end if;

			when INIT_CLEAR_DISPLAY =>
				if (busy = '0') then
					instr_wr <= '1';
					instr <= INSTR_CLEAR_SCREEN;
					instr_data <= COLOR_BLACK & COLOR_BLACK & x"00";
					next_state <= INIT_WAIT_CLEAR;
				end if;

			when INIT_WAIT_CLEAR =>
				if (busy = '0') then
					next_state <= PLACE_SNAKE;
				end if;

			when PLACE_SNAKE =>
				snake_init <= '1';
				movement_direction_next <= LEFT;
				next_state <= PLACE_SNAKE_WAIT;

			when PLACE_SNAKE_WAIT =>
				instr_wr <= snake_instr_wr;
				instr <= snake_instr;
				instr_data <= snake_instr_data;
				if(snake_done = '1') then
					if buf_game_state = RUNNING then
						next_state <= INIT_WAIT_CLEAR_FOOD_PLACER;
					else
						next_state <= WAIT_VBLANK;
					end if;
				end if;

			when INIT_WAIT_CLEAR_FOOD_PLACER =>
				if fp_busy = '0' then
					next_state		<= INIT_PLACING_FOOD;
				end if;

			when INIT_PLACING_FOOD =>
					fp_wr			<= '1';
					fp_sd_food		<= '1';
					fp_food_type	<= '0';
					next_state <= INIT_WAIT_FOOD_PLACER;

			when INIT_WAIT_FOOD_PLACER =>
				instr_wr 	<= fp_instr_wr;
				instr 		<= fp_instr;
				instr_data 	<= fp_instr_data;
				if(fp_done = '1') then
					next_state <= WAIT_VBLANK;
				end if;

			when WAIT_VBLANK =>
				if (vblank = '1' and prev_vblank = '0') then
				-- if (vblank = '1' ) then
					if (unsigned(frame_counter) = FRAMES_BETWEEN_MOVEMENTS-1) then
						frame_counter_next <= (others=>'0');
						next_state <= PROCESS_INPUTS;
					else
						frame_counter_next <= std_logic_vector(unsigned(frame_counter) + 1);
					end if;
				end if;


			when PROCESS_INPUTS =>
				movement_direction_next <= movement_direction;

				if buf_game_state = RUNNING then -- controlled movement
					if (cntrl1.btn_up = '1' or cntrl2.btn_c_up = '1') then
						tmp_dir := UP;
					elsif (cntrl1.btn_down = '1' or cntrl2.btn_c_down = '1') then
						tmp_dir := DOWN;
					elsif (cntrl1.btn_left = '1' or cntrl2.btn_c_left = '1') then
						tmp_dir := LEFT;
					elsif (cntrl1.btn_right = '1' or cntrl2.btn_c_right = '1') then
						tmp_dir := RIGHT;
					end if;
					-- if we are going e.g. UP we cannot go DOWN for the next move
					if (tmp_dir /= not movement_direction) then
						movement_direction_next <= tmp_dir;
					end if;

					if next_buf_game_state = PAUSED then
						next_state <= PAUSING;
					else
						next_state <= CURSOR_SET;
					end if;

				elsif buf_game_state = IDLE then -- random movement
					case rnd_vec is
						when "00" => tmp_dir := UP;
						when "01" => tmp_dir := DOWN;
						when "10" => tmp_dir := LEFT;
						when others => tmp_dir := RIGHT;
					end case;
					--skip_move is used to prevent the snake from eating its own tail
					skip_move_next <= not skip_move;
					if (tmp_dir /= not movement_direction and skip_move = '0') then
						movement_direction_next <= tmp_dir;
					end if;

					if next_buf_game_state = RUNNING then
						next_state <= RESET_GAME;
					else
						next_state <= MOVE_HEAD;
					end if;
				else
					next_state <= WAIT_VBLANK;
				end if;

			when PAUSING =>
				if not (buf_game_state = PAUSED) then
					next_state <= CURSOR_SET;
				end if;

			------------------------------------------------
			--         CHECKING NEW HEAD POSITION         --
			------------------------------------------------
			when CURSOR_SET =>
				if (busy = '0') then
					instr_wr	<= '1';
					instr		<= INSTR_SET_CURSOR_POSITION;
					instr_data	<= vec2d_to_txt_cntrl_data(move_vec2d_and_wrap_max(snake_head_position,movement_direction));
					next_state	<= CURSOR_REQUEST_CHAR;
				end if;

			when CURSOR_REQUEST_CHAR =>
				if (busy = '0') then
					instr_wr	<= '1';
					instr		<= INSTR_GET_CHAR;
					instr_data	<= (others => '0');
					next_state	<= CURSOR_CHECK;
				end if;

			when CURSOR_CHECK =>
				next_tail_movement <= '1';

				if (busy = '0') then
					case instr_result(15 downto 12) is
						when COLOR_BLUE =>
							if score > highscore then
								next_highscore <= score;
							end if;
							next_is_dead <= true;
							next_state <= DEAD;

						when FOOD_COLOR =>
							next_score <= score + FOOD_POINTS;
							next_tail_movement <= '0';
							next_sf_threshold_cnt <= sf_threshold_cnt + 1;
							if (sf_threshold_cnt + 1 = SPECIAL_FOOD_THRESHOLD) then
								next_sf_threshold_cnt <= 0;
								next_state <= PLACING_SPECIAL_FOOD;
							else
								next_state <= PLACING_FOOD;
							end if;

						when SPECIAL_FOOD_COLOR =>
							next_score <= score + SPECIAL_FOOD_POINTS;
							next_tail_movement <= '0';
							next_sf_cntdwn <= 0;
							next_state <= PLACING_FOOD;

						when others =>
							-- next_state <= SPECIAL_FOOD_HANDLER;
							if sf_cntdwn = 1 then
								next_state <= DELETE_SPECIAL_FOOD;
							else
								next_state <= MOVE_HEAD;
							end if;
					end case;
				end if;

			-----------------                             --
			----            PLACING FOOD                ----
			--                             -----------------


			when DELETE_SPECIAL_FOOD =>
				next_sf_cntdwn <= 0;
				next_sf_threshold_cnt <= 0;
				if fp_busy = '0' then
					fp_wr			<= '1';
					fp_sd_food		<= '0';
					next_state		<= WAIT_FOOD_PLACER_DELETE;
				end if;

			when WAIT_FOOD_PLACER_DELETE =>
				instr_wr 	<= fp_instr_wr;
				instr 		<= fp_instr;
				instr_data 	<= fp_instr_data;
				if(fp_done = '1') then
					next_state <= PLACING_FOOD;
				end if;

			when PLACING_FOOD =>
				if fp_busy = '0' then
					fp_wr			<= '1';
					fp_sd_food		<= '1';
					fp_food_type	<= '0';
					next_state		<= WAIT_FOOD_PLACER;
				end if;

			when PLACING_SPECIAL_FOOD =>
				if fp_busy = '0' then
					fp_wr			<= '1';
					fp_sd_food		<= '1';
					fp_food_type	<= '1';
					next_state		<= WAIT_FOOD_PLACER;
					next_sf_cntdwn  <= SPECIAL_FOOD_TIMEOUT_CLK_TICKS;
				end if;

			when WAIT_FOOD_PLACER =>
				instr_wr 	<= fp_instr_wr;
				instr 		<= fp_instr;
				instr_data 	<= fp_instr_data;
				if(fp_done = '1') then
					next_state <= MOVE_HEAD;
				end if;


			-----------------                             --
			----             SNAKE MOVEMENT             ----
			--                             -----------------
			when MOVE_HEAD =>
				snake_move_head <= '1';
				next_state <= WAIT_MOVE_HEAD;

			when WAIT_MOVE_HEAD =>
				instr_wr <= snake_instr_wr;
				instr <= snake_instr;
				instr_data <= snake_instr_data;
				if(snake_done = '1') then
					if tail_movement = '0' then
						next_state <= WAIT_VBLANK;
					else
						next_state <= MOVE_TAIL;
					end if;
				end if;

			when MOVE_TAIL =>
				snake_move_tail <= '1';
				next_state <= WAIT_MOVE_TAIL;

			when WAIT_MOVE_TAIL =>
				instr_wr <= snake_instr_wr;
				instr <= snake_instr;
				instr_data <= snake_instr_data;
				if(snake_done = '1') then
					next_state <= WAIT_VBLANK;
				end if;

			when DEAD =>
				if (buf_game_state = GAME_OVER) and not (next_buf_game_state = GAME_OVER) then
					next_state <= RESET_GAME;
				end if;

			when others =>
				null;
		end case;

		-- if game_state is changed from IDLE to RUNNING
		-- the snake should be reseted and food should be placed
		if (buf_game_state = IDLE) and (next_buf_game_state = RUNNING) then
			next_state <= RESET_GAME;
		end if;
	end process;



	snake_inst : snake
		port map (
			clk					=> clk,
			res_n				=> res_n,
			tm_instr_wr			=> snake_instr_wr,
			tm_instr			=> snake_instr,
			tm_instr_data		=> snake_instr_data,
			tm_busy				=> busy,
			tm_instr_result		=> instr_result,
			color				=> COLOR_BLUE & COLOR_BLACK,
			init				=> snake_init,
			init_head_position	=> INITIAL_SNAKE_HEAD_POSITION,
			init_head_direction	=> INITIAL_SNAKE_HEAD_DIRECTION,
			init_body_length	=> INITIAL_SNAKE_BODY_LENGTH,
			head_position		=> snake_head_position,
			movement_direction	=> movement_direction,
			move_head			=> snake_move_head,
			move_tail			=> snake_move_tail,
			done				=> snake_done
		);

	prng_inst0 : prng
		port map (
			clk    => clk,
			res_n  => res_n,
			load_seed => load_seed,
			seed   => (others=>'0'),
			en     => '1',
			prdata => rnd_vec(0)
		);

	prng_inst1 : prng
		port map (
			clk    => clk,
			res_n  => res_n,
			load_seed => load_seed,
			seed   => x"42",
			en     => '1',
			prdata => rnd_vec(1)
		);


	food_placer_inst : food_placer
		port map (
			clk				=> clk,
			res_n			=> res_n,
			wr				=> fp_wr,
			sd_food			=> fp_sd_food,
			food_type		=> fp_food_type,
			busy			=> fp_busy,
			done			=> fp_done,
			tm_instr_wr		=> fp_instr_wr,
			tm_instr		=> fp_instr,
			tm_instr_data	=> fp_instr_data,
			tm_instr_result	=> instr_result,
			tm_busy			=> busy

		);


	sound_maker_inst : sound_maker
		port map (
			clk				=> clk,
			res_n			=> res_n,
			sg_score		=> score,
			sg_game_state	=> buf_game_state,

			--connection to the audio controller
			synth_cntrl		=> synth_cntrl
		);

	-- synth_cntrl(0).high_time <= x"20";
	-- synth_cntrl(0).low_time <= x"20";
	-- synth_cntrl(0).play <= cntrl1.btn_up or cntrl1.btn_down;
	--
	-- synth_cntrl(1).high_time <= x"10";
	-- synth_cntrl(1).low_time <= x"10";
	-- synth_cntrl(1).play <= cntrl1.btn_left or cntrl1.btn_right;

	games_state_handler : process(all)
	begin
		next_buf_game_state <= buf_game_state;

		case buf_game_state is
			when IDLE		=>
				if (cntrl1.btn_a = '1' or cntrl2.btn_a = '1') then
					next_buf_game_state <= RUNNING;
				end if;

			when RUNNING	=>
				if (is_dead) then
					next_buf_game_state <= GAME_OVER;
				else
					if (cntrl1.btn_start = '1' or cntrl2.btn_start = '1') then
						next_buf_game_state <= PAUSED;
					end if;
				end if;

			when PAUSED		=>
				if (cntrl1.btn_start = '1' or cntrl2.btn_start = '1') then
					next_buf_game_state <= RUNNING;
				end if;

			when GAME_OVER	=>
				if (cntrl1.btn_start = '1' or cntrl2.btn_start = '1') then
					next_buf_game_state <= IDLE;
				end if;
		end case;
	end process;

	-- char_lcd_instr_wr <= '0';
	-- char_lcd_instr <= (others=>'0');
	-- char_lcd_instr_data <= (others=>'0');

	lcd_writer_inst : lcd_writer
	port map(
		clk    					=> clk,
		res_n  					=> res_n,
		sg_score				=> score,
		sg_highscore			=> highscore,
		sg_game_state			=> buf_game_state,

		char_lcd_instr_wr		=> char_lcd_instr_wr,
		char_lcd_instr			=> char_lcd_instr,
		char_lcd_instr_data		=> char_lcd_instr_data,
		char_lcd_instr_result	=> char_lcd_instr_result,
		char_lcd_busy			=> char_lcd_busy
	);

end architecture;
