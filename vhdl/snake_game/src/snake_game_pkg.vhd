library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.n64_controller_pkg.all;
use work.audio_cntrl_pkg.all;
use work.snake_pkg.all;


package snake_game_pkg is

	type snake_game_state_t is (IDLE, PAUSED, RUNNING, GAME_OVER);

	constant FRAMES_BETWEEN_MOVEMENTS		: integer := 6;
	constant INITIAL_SNAKE_HEAD_DIRECTION	: direction_t := LEFT;
	constant INITIAL_SNAKE_HEAD_POSITION	: vec2d_t := create_vec2d(10,10);
	constant INITIAL_SNAKE_BODY_LENGTH		: std_logic_vector(3 downto 0) := x"5";
	constant FOOD_POINTS					: integer := 1;
	constant SPECIAL_FOOD_THRESHOLD			: integer := 2;
	constant SPECIAL_FOOD_POINTS			: integer := 5;
	constant SPECIAL_FOOD_TIMEOUT_MS		: integer := 10000;

	constant ONE_NS : integer := 1_000_000_000/25_000_000;

	type numbers_t is array (0 to 9) of Character;
	constant numbers_table : numbers_t := (	'0', '1', '2', '3', '4',
											'5', '6', '7', '8', '9');

	function to_str ( a : integer; len : integer) return string;
	function conv_to_str ( a : integer; len : integer) return string;

	component snake_game is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			instr_wr : out std_logic;
			instr : out std_logic_vector(3 downto 0);
			instr_data : out std_logic_vector(15 downto 0);
			instr_result : in std_logic_vector(15 downto 0);
			busy : in std_logic;
			vblank : in std_logic;
			cntrl1 : in n64_buttons_t;
			cntrl2 : in n64_buttons_t;
			synth_cntrl : out synth_cntrl_vec_t(0 to 1);
			char_lcd_instr_wr : out std_logic;
			char_lcd_instr : out std_logic_vector(3 downto 0);
			char_lcd_instr_data : out std_logic_vector(15 downto 0);
			char_lcd_instr_result : in std_logic_vector(15 downto 0);
			char_lcd_busy : in std_logic;
			game_state : out snake_game_state_t
		);
	end component;

end package;

package body snake_game_pkg is

	function to_str ( a : integer; len : integer) return string is
		variable str : string (1 to len) := (others => '0');
		variable b : integer := a;
	begin
		for i in len downto 1 loop
				str(len-(i-1)) := integer'image(b/10**(i-1))(1);
				-- str(len-(i-1)) := numbers_table(b/10**(i-1));
				b := b - (b/10**(i-1))*10**(i-1);
		end loop;
	return str;
	end function;

	function conv_to_str ( a : integer; len : integer) return string is
		variable str : string (1 to len) := (others => '0');
		variable b : integer := a;
	begin
		str := str(1 to len-integer'image(a)'length) & integer'image(a);
	return str;
	end function;

end package body;
