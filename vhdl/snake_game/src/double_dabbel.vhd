library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use work.lcd_writer_pkg.all;

entity double_dabble is
	Port (
		binIN : in  STD_LOGIC_VECTOR (19 downto 0);
		digits : out bcd_numbers_t
	);
end double_dabble;

architecture Behavioral of double_dabble is

	begin

		bcd1: process(binIN)

		-- temporary variable
		variable temp : STD_LOGIC_VECTOR (19 downto 0);

		-- variable to store the output BCD number
		-- organized as follows
		-- tenthousands = bcd(19 downto 16)
		-- thousands = bcd(15 downto 12)
		-- hundreds = bcd(11 downto 8)
		-- tens = bcd(7 downto 4)
		-- units = bcd(3 downto 0)
		variable bcd : UNSIGNED (19 downto 0) := (others => '0');

		-- by
		-- https://en.wikipedia.org/wiki/Double_dabble
		-- ^^^^^^    STOLEN AND ADAPTED FROM    ^^^^^^

		begin
			-- zero the bcd variable
			bcd := (others => '0');

			-- read input into temp variable
			temp(19 downto 0) := binIN;

			-- cycle 12 times as we have 12 input bits
			-- this could be optimized, we do not need to check and add 3 for the
			-- first 3 iterations as the number can never be >4
			for i in 0 to 19 loop

				if bcd(3 downto 0) > 4 then
					bcd(3 downto 0) := bcd(3 downto 0) + 3;
				end if;

				if bcd(7 downto 4) > 4 then
					bcd(7 downto 4) := bcd(7 downto 4) + 3;
				end if;

				if bcd(11 downto 8) > 4 then
					bcd(11 downto 8) := bcd(11 downto 8) + 3;
				end if;

				if bcd(15 downto 12) > 4 then
					bcd(15 downto 12) := bcd(15 downto 12) + 3;
				end if;

				if bcd(19 downto 16) > 4 then
					bcd(19 downto 16) := bcd(19 downto 16) + 3;
				end if;

				-- thousands can't be >4 for a 12-bit input number
				-- so don't need to do anything to upper 4 bits of bcd

				-- shift bcd left by 1 bit, copy MSB of temp into LSB of bcd
				bcd := bcd(18 downto 0) & temp(19);

				-- shift temp left by 1 bit
				temp := temp(18 downto 0) & '0';

			end loop;

			-- set outputs
			digits(0) <= STD_LOGIC_VECTOR(bcd(3 downto 0));
			digits(1) <= STD_LOGIC_VECTOR(bcd(7 downto 4));
			digits(2) <= STD_LOGIC_VECTOR(bcd(11 downto 8));
			digits(3) <= STD_LOGIC_VECTOR(bcd(15 downto 12));
			digits(4) <= STD_LOGIC_VECTOR(bcd(19 downto 16));

		end process bcd1;

	end Behavioral;
