library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.char_lcd_cntrl_pkg.all;
use work.lcd_writer_pkg.all;
use work.textmode_controller_pkg.all;
use work.display_controller_pkg.all;
use work.snake_game_pkg.all;

entity lcd_writer is
	port (
		clk				: in	std_logic;   -- global system clk
		res_n			: in	std_logic;   -- system reset

		-- communication with the snake_game module
		-- wr				: std_logic;
		-- lcd_text		: in string(1 to 32);
		sg_score		: integer;
		sg_highscore	: integer;
		sg_game_state	: snake_game_state_t;
		-- busy			: out std_logic;

		-- connection to the character LCD controller in the snake_game
		char_lcd_instr_wr     : out std_logic;
		char_lcd_instr        : out std_logic_vector(3 downto 0);
		char_lcd_instr_data   : out std_logic_vector(15 downto 0);
		char_lcd_instr_result : in std_logic_vector(15 downto 0);
		char_lcd_busy         : in std_logic
	);
end entity;


architecture beh of lcd_writer is

	component double_dabble is
		Port (
			binIN	: in  STD_LOGIC_VECTOR (19 downto 0);
			digits	: out bcd_numbers_t
		);
	end component;

	type LCD_WRITER_STATE is (INIT_CFG, IDLE, WRITE_CHAR, WAIT_BUSY--);
								,CURSOR_HOME, CLEAR_DISPLAY);

	signal state 						: LCD_WRITER_STATE;
	signal next_state 					: LCD_WRITER_STATE;

	signal return_state					: LCD_WRITER_STATE;
	signal next_return_state			: LCD_WRITER_STATE;

	signal clk_cnt 						: integer := 0;
	signal next_clk_cnt					: integer := 0;

	-- COLUMN_CNT and ROW_CNT are constants in the char_lcd_cntrl_pkg
	signal char_cnt 					: integer range 1 to CHAR_NBR;
	signal next_char_cnt 				: integer range 1 to CHAR_NBR;

	signal buf_sg_score					: integer := 0;
	signal next_buf_sg_score			: integer;
	signal buf_sg_highscore				: integer := 0;
	signal next_buf_sg_highscore		: integer;
	signal buf_sg_game_state			: snake_game_state_t := IDLE;
	signal next_buf_sg_game_state		: snake_game_state_t;

	signal buf_char_lcd_instr 			: std_logic_vector(3 downto 0);
	signal next_buf_char_lcd_instr 		: std_logic_vector(3 downto 0);
	signal buf_char_lcd_instr_data 		: std_logic_vector(15 downto 0);
	signal next_buf_char_lcd_instr_data	: std_logic_vector(15 downto 0);

	signal digits_score					: bcd_numbers_t;
	signal digits_highscore				: bcd_numbers_t;
begin



	double_dabble_score_inst : double_dabble
		port map(
			binIN		=> std_logic_vector(to_unsigned(sg_score,20)),
			digits		=> digits_score
		);

	double_dabble_highscore_inst : double_dabble
		port map(
			binIN		=> std_logic_vector(to_unsigned(sg_highscore,20)),
			digits		=> digits_highscore
		);

-- -------------------------------------------------------
-- --					SYNC	PROCESS					--
-- -------------------------------------------------------
	sync : process(clk, res_n)
	begin
		if res_n = '0' then -- reset
			-- reset counter signals
			clk_cnt 				<= 0;
			state					<= INIT_CFG;
			return_state			<= IDLE;
			buf_char_lcd_instr		<= INSTR_NOP;
			buf_char_lcd_instr_data	<= (others => '0');
			char_cnt				<= 1;

			buf_sg_game_state		<= IDLE;
			buf_sg_score			<= 0;
			buf_sg_highscore		<= 0;

			-- remainder				<= 0;
		elsif rising_edge(clk) then
			clk_cnt 				<= next_clk_cnt;
			state 					<= next_state;
			return_state			<= next_return_state;
			char_cnt				<= next_char_cnt;
			buf_char_lcd_instr 		<= next_buf_char_lcd_instr;
			buf_char_lcd_instr_data <= next_buf_char_lcd_instr_data;

			buf_sg_score			<= next_buf_sg_score;
			buf_sg_highscore		<= next_buf_sg_highscore;
			buf_sg_game_state		<= next_buf_sg_game_state;

		end if;
	end process sync;

	-- -------------------------------------------------------
	-- --				NEXT	STATE	LOGIC				--
	-- -------------------------------------------------------
		logic_next_state : process(all)
		begin
			next_state						<= state;
			next_return_state				<= IDLE;
			next_clk_cnt					<= clk_cnt;
			next_char_cnt					<= char_cnt;
			next_buf_char_lcd_instr			<= buf_char_lcd_instr;
			next_buf_char_lcd_instr_data	<= buf_char_lcd_instr_data;

			next_buf_sg_score				<= buf_sg_score;
			next_buf_sg_highscore			<= buf_sg_highscore;
			next_buf_sg_game_state			<= buf_sg_game_state;

			char_lcd_instr_wr				<= '0';
			char_lcd_instr					<= buf_char_lcd_instr;
			char_lcd_instr_data				<= buf_char_lcd_instr_data;



			case( state ) is
				when INIT_CFG	=>
					next_buf_char_lcd_instr			<= INSTR_CFG;
					next_buf_char_lcd_instr_data	<= x"0008";
					next_state 						<= WAIT_BUSY;
					next_return_state				<= CURSOR_HOME;

				when IDLE 		=>
					next_char_cnt <= 1;

					if (not (sg_game_state = buf_sg_game_state) or
						not (sg_score = buf_sg_score) or
						not (sg_highscore = buf_sg_highscore)) then

							next_buf_sg_score		<= sg_score;
							next_buf_sg_highscore	<= sg_highscore;
							next_buf_sg_game_state	<= sg_game_state;

							next_state <= CURSOR_HOME;
					end if;


				when CURSOR_HOME =>
					next_buf_char_lcd_instr			<= INSTR_SET_CURSOR_POSITION;
					next_buf_char_lcd_instr_data	<= x"0000";
					next_return_state 				<= WRITE_CHAR;
					next_state 						<= WAIT_BUSY;


				when CLEAR_DISPLAY => --unused (initialy added to see if screen refreshes)
					next_buf_char_lcd_instr			<= INSTR_CLEAR_SCREEN;
					next_buf_char_lcd_instr_data	<= x"0000";
					next_return_state 				<= WRITE_CHAR;
					next_state 						<= WAIT_BUSY;



				when WRITE_CHAR	=>
					next_buf_char_lcd_instr			<= INSTR_SET_CHAR;

					if buf_sg_game_state = IDLE then
						if char_cnt > CHAR_NBR-5 then -- for highscore
						-- if CHAR_NBR - char_cnt < 5 then -- for highscore
							next_buf_char_lcd_instr_data	<= x"00" & ASCII_NUMBERS_TAB(to_integer(unsigned(digits_highscore(CHAR_NBR-char_cnt))));
						else -- for text
							next_buf_char_lcd_instr_data	<= x"00" & DEFAULT_IDLE_TEXT(char_cnt);
						end if;
					else
						if char_cnt > COLUMN_CNT - 5 and  char_cnt <= COLUMN_CNT then -- for the score
							next_buf_char_lcd_instr_data	<= x"00" & ASCII_NUMBERS_TAB(to_integer(unsigned(digits_score(CHAR_NBR-char_cnt))));
						elsif char_cnt > CHAR_NBR - 5 then -- for highscore
							next_buf_char_lcd_instr_data	<= x"00" & ASCII_NUMBERS_TAB(to_integer(unsigned(digits_highscore(CHAR_NBR-char_cnt))));
						else -- for text
							next_buf_char_lcd_instr_data	<= x"00" & DEFAULT_OTHERS_TEXT(char_cnt);
						end if;
					end if;

					if char_cnt = CHAR_NBR then
						next_char_cnt 		<= 1;
					else
						next_return_state	<= WRITE_CHAR;
						next_char_cnt		<= char_cnt + 1;
					end if;
					next_state 				<= WAIT_BUSY;



				when WAIT_BUSY	=>
					next_return_state <= return_state;
					if char_lcd_busy = '0' then
						char_lcd_instr_wr <= '1';
						next_state <= return_state;
					end if;
			end case;
		end process logic_next_state;


end architecture;
