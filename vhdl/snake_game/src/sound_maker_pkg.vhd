library ieee;
use ieee.std_logic_1164.all;
use work.math_pkg.all;

use work.audio_cntrl_pkg.all;
use work.snake_game_pkg.all;

package sound_maker_pkg is

	component sound_maker is
		port (
			clk				: in	std_logic;   -- global system clk
			res_n			: in	std_logic;   -- system reset

			-- communication with the snake_game module
			sg_score		: in integer;
			sg_game_state	: in snake_game_state_t;

			--connection to the audio controller
			synth_cntrl		: out synth_cntrl_vec_t(0 to 1)
		);
	end component;

end package;
