library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

use work.audio_cntrl_pkg.all;
use work.math_pkg.all;
use work.snake_game_pkg.all;

entity sound_maker is
	port (
		clk				: in	std_logic;   -- global system clk
		res_n			: in	std_logic;   -- system reset

		-- communication with the snake_game module
		sg_score		: in integer;
		sg_game_state	: in snake_game_state_t;

		--connection to the audio controller
		synth_cntrl : out synth_cntrl_vec_t(0 to 1)
	);
end entity;


architecture beh of sound_maker is


	type SOUND_MAKER_STATE_T is (IDLE, TONE_NORMAL_FOOD, TONE_SPECIAL_FOOD, TONE_GAME_OVER_LOW, TONE_GAME_OVER_HIGH);

	signal state 						: SOUND_MAKER_STATE_T;
	signal next_state 					: SOUND_MAKER_STATE_T;

	signal clk_cnt 						: integer := 0;
	signal next_clk_cnt					: integer := 0;

	signal buf_sg_score					: integer := 0;
	signal next_buf_sg_score			: integer;
	
	signal buf_sg_game_state			: snake_game_state_t := IDLE;
	signal next_buf_sg_game_state		: snake_game_state_t;

begin


synth_cntrl(0).high_time <= x"80";
synth_cntrl(0).low_time <= x"80";

synth_cntrl(1).high_time <= x"20";
synth_cntrl(1).low_time <= x"20";

-- -------------------------------------------------------
-- --					SYNC	PROCESS					--
-- -------------------------------------------------------
	sync : process(clk, res_n)
	begin
		if res_n = '0' then -- reset
			-- reset counter signals
			clk_cnt 				<= 0;
			state					<= IDLE;

			buf_sg_game_state		<= IDLE;
			buf_sg_score			<= 0;
		elsif rising_edge(clk) then
			clk_cnt 				<= next_clk_cnt;
			state 					<= next_state;

			buf_sg_score			<= next_buf_sg_score;
			buf_sg_game_state		<= next_buf_sg_game_state;
		end if;
	end process sync;

	-- -------------------------------------------------------
	-- --				NEXT	STATE	LOGIC				--
	-- -------------------------------------------------------
		logic_next_state : process(all)
		begin
			next_state						<= state;
			next_clk_cnt					<= clk_cnt;

			next_buf_sg_score				<= buf_sg_score;
			next_buf_sg_game_state			<= buf_sg_game_state;

			synth_cntrl(0).play <= '0';
			synth_cntrl(1).play <= '0';



			case( state ) is
				when IDLE 		=>
					next_clk_cnt <= 0;
					next_buf_sg_score		<= sg_score;
					next_buf_sg_game_state	<= sg_game_state;

					if sg_score - buf_sg_score = FOOD_POINTS then
						-- normal food tone
						next_state <= TONE_NORMAL_FOOD;
					elsif sg_score - buf_sg_score = SPECIAL_FOOD_POINTS then
						-- special food tone
						next_state <= TONE_SPECIAL_FOOD;
					end if;

					if sg_game_state = GAME_OVER and not (buf_sg_game_state = GAME_OVER) then
						-- play dead  tones
						next_state <= TONE_GAME_OVER_LOW;
					end if;


				when TONE_NORMAL_FOOD =>
					synth_cntrl(0).play <= '1';
					next_clk_cnt <= clk_cnt + 1;
					if clk_cnt*ONE_NS > 200_000_000 then
						next_state <= IDLE;
						next_clk_cnt <= 0;
					end if;


				when TONE_SPECIAL_FOOD =>
					synth_cntrl(1).play <= '1';
					next_clk_cnt <= clk_cnt + 1;
					if clk_cnt*ONE_NS > 200_000_000 then
						next_state <= IDLE;
						next_clk_cnt <= 0;
					end if;


				when TONE_GAME_OVER_LOW =>
					synth_cntrl(0).play <= '1';
					next_clk_cnt <= clk_cnt + 1;
					if clk_cnt*ONE_NS > 250_000_000 then
						next_state <= TONE_GAME_OVER_HIGH;
						next_clk_cnt <= 0;
					end if;


				when TONE_GAME_OVER_HIGH =>
					synth_cntrl(1).play <= '1';
					next_clk_cnt <= clk_cnt + 1;
					if clk_cnt*ONE_NS > 250_000_000 then
						next_state <= IDLE;
						next_clk_cnt <= 0;
					end if;


			end case;
		end process logic_next_state;


end architecture;
