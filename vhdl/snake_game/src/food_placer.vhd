library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

use work.snake_pkg.all;
use work.textmode_controller_pkg.all;
use work.display_controller_pkg.all;
use work.math_pkg.all;
use work.char_lcd_cntrl_pkg.all;
use work.lcd_writer_pkg.all;
use work.snake_game_pkg.all;
use work.prng_pkg.all;
use work.food_placer_pkg.all;

entity food_placer is
	port (
		clk				: in	std_logic;   -- global system clk
		res_n			: in	std_logic;   -- system reset

		-- communication with the snake_game module
		wr			: in std_logic;
		sd_food		: in std_logic;	-- 1 for setting and 0 for deleting food
		food_type	: in std_logic;	-- 1 for special and 0 for normal food
		busy		: out std_logic;
		done		: out std_logic;

		-- connection to the character LCD controller in the snake_game
		tm_instr_wr     : out std_logic;
		tm_instr        : out std_logic_vector(3 downto 0);
		tm_instr_data   : out std_logic_vector(15 downto 0);
		tm_instr_result : in std_logic_vector(15 downto 0);
		tm_busy         : in std_logic
	);
end entity;


architecture beh of food_placer is
	-- constant FOOD_CHAR			: std_logic_vector(7 downto 0) := x"FF";

	signal rnd_vec				: std_logic_vector(1 downto 0);
	signal load_seed 			: std_logic;

	type FOOD_PLACER_STATE is (RESET, IDLE, CURSOR_SET, CURSOR_CHECK,
								CHANGE_POSITON, SET_CHAR, COMPLETE);

	signal state 				: FOOD_PLACER_STATE;
	signal next_state 			: FOOD_PLACER_STATE;

	signal clk_cnt 				: integer := 0;
	signal next_clk_cnt			: integer := 0;

	signal rand					: vec2d_t;
	signal next_rand			: vec2d_t;

	signal rand_pos				: vec2d_t;
	signal next_rand_pos		: vec2d_t;

	signal sf					: vec2d_t; -- special food position
	signal next_sf				: vec2d_t;

	signal buf_sd_food			: std_logic;
	signal next_buf_sd_food		: std_logic;

	signal buf_food_type		: std_logic;
	signal next_buf_food_type	: std_logic;

begin


-- -------------------------------------------------------
-- --					SYNC	PROCESS					--
-- -------------------------------------------------------
	sync : process(clk, res_n)
	begin
		if res_n = '0' then -- reset
			state					<= RESET;
			clk_cnt 				<= 0;
			rand_pos				<= create_vec2d(0,0);
			rand					<= create_vec2d(0,0);
			sf						<= NULL_VEC;

			buf_sd_food	 			<= '0';
			buf_food_type			<= '0';

		elsif rising_edge(clk) then
			state 					<= next_state;
			clk_cnt 				<= next_clk_cnt;
			rand_pos 				<= next_rand_pos;
			rand 					<= next_rand;
			sf						<= next_sf;

			buf_sd_food				<= next_buf_sd_food; -- 0 for delete and 1 for set
			buf_food_type			<= next_buf_food_type;
		end if;
	end process sync;

	-- -------------------------------------------------------
	-- --				NEXT	STATE	LOGIC				--
	-- -------------------------------------------------------
	logic_next_state : process(all)
	begin
		load_seed <= '0';

		next_state			<= state;
		next_clk_cnt		<= clk_cnt;

		next_rand_pos		<= rand_pos;
		next_sf				<= sf;

		next_buf_sd_food	<= buf_sd_food;
		next_buf_food_type	<= buf_food_type;

		busy				<= '1';
		done				<= '0';
		tm_instr_wr			<= '0';
		tm_instr			<= INSTR_NOP;
		tm_instr_data		<= (others=>'0');

		next_rand <= (	x => (rand.x(VEC2D_X_WIDTH-2 downto 0) & rnd_vec(1)),
						y => (rand.y(VEC2D_Y_WIDTH-2 downto 0) & rnd_vec(0)));

		case( state ) is
			when RESET =>
				load_seed <= '1';
				next_state <= IDLE;

			when IDLE =>
				busy <= '0';
				if wr = '1' then
					next_rand_pos 		<= adj_food(rand);

					next_buf_sd_food	<= sd_food;
					next_buf_food_type	<= food_type;

					next_state			<= CURSOR_SET;
				end if;

			when CURSOR_SET =>
				if (tm_busy = '0') then
						tm_instr_wr <= '1';
						tm_instr <= INSTR_SET_CURSOR_POSITION;

						if buf_sd_food = '0' then
							if sf = NULL_VEC then
								next_state <= COMPLETE;
							else
								tm_instr_data	<= vec2d_to_txt_cntrl_data(sf);
								next_state <= SET_CHAR;
							end if;
						else
							tm_instr_data	<= vec2d_to_txt_cntrl_data(rand_pos);
							next_state <= CURSOR_CHECK;
						end if;
				end if;

			when CURSOR_CHECK =>
				if (tm_busy = '0') then
					tm_instr_wr <= '1';
					tm_instr <= INSTR_GET_CHAR;
					next_state <= CHANGE_POSITON;
				end if;

			when CHANGE_POSITON =>
				if (tm_busy = '0') then
					if tm_instr_result(15 downto 12) = COLOR_BLACK then
						next_state <= SET_CHAR;
					else
						next_rand_pos <= add(rand_pos, create_vec2d(1,0) );
						next_state <= CURSOR_SET;
					end if;
				end if;

			when SET_CHAR =>
				if (tm_busy = '0') then
					tm_instr_wr <= '1';
					tm_instr <= INSTR_SET_CHAR;

					if (buf_sd_food = '0') then
						tm_instr_data <= (others => '0');
						next_sf <= NULL_VEC;
					else
						if (buf_food_type = '1') then
							tm_instr_data <= SPECIAL_FOOD_COLOR & SPECIAL_FOOD_COLOR & FOOD_CHAR;
							next_sf <= rand_pos;
						else
							tm_instr_data <= FOOD_COLOR & FOOD_COLOR & FOOD_CHAR;
						end if;
					end if;

					next_state <= COMPLETE;
				end if;

			when COMPLETE =>
				done <= '1';
				next_state <= IDLE;

		end case;
	end process logic_next_state;



	prng_inst0 : prng
	port map (
		clk    => clk,
		res_n  => res_n,
		load_seed => load_seed,
		seed   => x"13",
		en     => '1',
		prdata => rnd_vec(0)
	);

	prng_inst1 : prng
	port map (
		clk    => clk,
		res_n  => res_n,
		load_seed => load_seed,
		seed   => x"69",
		en     => '1',
		prdata => rnd_vec(1)
	);

end architecture;
