library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.snake_game_pkg.all;
use work.char_lcd_cntrl_pkg.all;

package lcd_writer_pkg is

	component lcd_writer is
		port (
			clk				: in	std_logic;   -- global system clk
			res_n			: in	std_logic;   -- system reset

			-- communication with the snake_game module
			-- wr : std_logic;
			-- lcd_text	: in string(1 to 32);
			sg_score		: integer;
			sg_highscore	: integer;
			sg_game_state	: snake_game_state_t;
			-- busy		: out std_logic;

			-- connection to the character LCD controller in the snake_game
			char_lcd_instr_wr     : out std_logic;
			char_lcd_instr        : out std_logic_vector(3 downto 0);
			char_lcd_instr_data   : out std_logic_vector(15 downto 0);
			char_lcd_instr_result : in std_logic_vector(15 downto 0);
			char_lcd_busy         : in std_logic
		);
	end component;

	constant CHAR_NBR			: integer := COLUMN_CNT*ROW_CNT;

	type bcd_numbers_t is array (0 to 4) of std_logic_vector(3 downto 0);

	type txt_vec_t is array(1 to  CHAR_NBR) of std_logic_vector(7 downto 0);
	constant DEFAULT_IDLE_TEXT		: txt_vec_t := (
			x"20", --
			x"20", --
			x"20", --
			x"44", -- D
			x"44", -- D
			x"43", -- C
			x"41", -- A
			x"20", --
			x"53", -- S
			x"4E", -- N
			x"41", -- A
			x"4B", -- K
			x"45", -- E
			x"20", --
			x"20", --
			x"20", --
			x"48", -- H
			x"69", -- i
			x"67", -- g
			x"68", -- h
			x"20", --
			x"53", -- S
			x"63", -- c
			x"6F", -- o
			x"72", -- r
			x"65", -- e
			x"3A", -- :
			x"30", -- 0
			x"30", -- 0
			x"30", -- 0
			x"30", -- 0
			x"30"  -- 0
		);
	constant DEFAULT_OTHERS_TEXT	: txt_vec_t := (
			x"53", -- S
			x"63", -- c
			x"6F", -- o
			x"72", -- r
			x"65", -- e
			x"3A", -- :
			x"20", --
			x"20", --
			x"20", --
			x"20", --
			x"20", --
			x"30", -- 0
			x"30", -- 0
			x"30", -- 0
			x"30", -- 0
			x"30", -- 0
			x"48", -- H
			x"69", -- i
			x"67", -- g
			x"68", -- h
			x"20", --
			x"53", -- S
			x"63", -- c
			x"6F", -- o
			x"72", -- r
			x"65", -- e
			x"3A", -- :
			x"30", -- 0
			x"30", -- 0
			x"30", -- 0
			x"30", -- 0
			x"30"  -- 0
		);

	type nbr_vec_t is array(0 to  9) of std_logic_vector(7 downto 0);
	constant ASCII_NUMBERS_TAB : nbr_vec_t := (
			x"30", -- 0
			x"31", -- 1
			x"32", -- 2
			x"33", -- 3
			x"34", -- 4
			x"35", -- 5
			x"36", -- 6
			x"37", -- 7
			x"38", -- 8
			x"39"  -- 9
		);

end package;
