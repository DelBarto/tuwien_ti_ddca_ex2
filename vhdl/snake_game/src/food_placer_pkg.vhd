library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

use work.math_pkg.all;
use work.snake_pkg.all;
use work.textmode_controller_pkg.all;
use work.display_controller_pkg.all;

package food_placer_pkg is

	constant FOOD_CHAR			: std_logic_vector(7 downto 0) := x"23";
	constant FOOD_COLOR			: std_logic_vector(3 downto 0) := COLOR_RED;
	constant SPECIAL_FOOD_COLOR	: std_logic_vector(3 downto 0) := COLOR_YELLOW;

	function append(l : vec2d_t; r : std_logic_vector(1 downto 0)) return vec2d_t;
	function adj_foodx(x : std_logic_vector) return std_logic_vector;
	function adj_foody(y : std_logic_vector) return std_logic_vector;
	function adj_food (f : vec2d_t) return vec2d_t;

	component food_placer is
		port (
			clk				: in	std_logic;   -- global system clk
			res_n			: in	std_logic;   -- system reset

			-- communication with the snake_game module
			wr			: in std_logic;
			sd_food		: in std_logic;
			food_type	: in std_logic;
			busy		: out std_logic;
			done		: out std_logic;

			-- connection to the character LCD controller in the snake_game
			tm_instr_wr     : out std_logic;
			tm_instr        : out std_logic_vector(3 downto 0);
			tm_instr_data   : out std_logic_vector(15 downto 0);
			tm_instr_result : in std_logic_vector(15 downto 0);
			tm_busy         : in std_logic
		);
	end component;

end package;

package body food_placer_pkg is

	function append(l : vec2d_t; r : std_logic_vector(1 downto 0)) return vec2d_t is
		constant vec : vec2d_t := (	x => (l.x(VEC2D_X_WIDTH-2 downto 0) & r(1)),
									y => (l.y(VEC2D_Y_WIDTH-2 downto 0) & r(0)));
	begin
		return vec;
	end function;


	function adj_foodx( x : std_logic_vector) return std_logic_vector is
		variable r : unsigned(VEC2D_X_WIDTH+2 downto 0) := (others => '0');
		variable a : unsigned(VEC2D_X_WIDTH+2 downto 0) := (others => '0');
	begin
		a(VEC2D_X_WIDTH-1 downto 0) := unsigned(x);
		r := a;
		r := shift_left(r, 2);
		r := r + a;
		r := shift_right(r, 3);
	return std_logic_vector(r(VEC2D_X_WIDTH-1 downto 0));
	end function;

	function adj_foody( y : std_logic_vector) return std_logic_vector is
		variable r : unsigned(VEC2D_Y_WIDTH+1 downto 0) := (others => '0');
		variable a : unsigned(VEC2D_Y_WIDTH+1 downto 0) := (others => '0');
	begin
		a(VEC2D_Y_WIDTH-1 downto 0) := unsigned(y);
		r := a;
		r := shift_left(r, 1);
		r := r + a;
		r := shift_right(r, 2);
	return std_logic_vector(r(VEC2D_Y_WIDTH-1 downto 0));
	end function;


	function adj_food (f : vec2d_t) return vec2d_t is
		constant mod_f : vec2d_t := (x => adj_foodx(f.x), y => adj_foody(f.y));
	BEGIN
		return mod_f;
	end function;

end package body;
