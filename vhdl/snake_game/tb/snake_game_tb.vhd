-- TASK 5: put your code here

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;
use work.snake_game_pkg.all;
use work.food_placer_pkg.all;
use work.snake_pkg.all;
use work.n64_controller_pkg.all;
use work.audio_cntrl_pkg.all;
use work.char_lcd_cntrl_pkg.all;
use work.display_controller_pkg.all;
use work.lcd_writer_pkg.all;

entity snake_game_tb is
end entity;

architecture bench of snake_game_tb is

	component snake_game is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			instr_wr : out std_logic;
			instr : out std_logic_vector(3 downto 0);
			instr_data : out std_logic_vector(15 downto 0);
			instr_result : in std_logic_vector(15 downto 0);
			busy : in std_logic;
			vblank : in std_logic;
			cntrl1 : in n64_buttons_t;
			cntrl2 : in n64_buttons_t;
			synth_cntrl : out synth_cntrl_vec_t(0 to 1);
			char_lcd_instr_wr : out std_logic;
			char_lcd_instr : out std_logic_vector(3 downto 0);
			char_lcd_instr_data : out std_logic_vector(15 downto 0);
			char_lcd_instr_result : in std_logic_vector(15 downto 0);
			char_lcd_busy : in std_logic;
			game_state : out snake_game_state_t
		);
	end component;

	signal clk				: std_logic;
	signal res_n			: std_logic;
	signal instr_wr			: std_logic;
	signal instr			: std_logic_vector(3 downto 0);
	signal instr_data		: std_logic_vector(15 downto 0);
	signal instr_result		: std_logic_vector(15 downto 0);
	signal busy				: std_logic;
	signal vblank			: std_logic;

	signal cntrl1					: n64_buttons_t;
	signal cntrl2					: n64_buttons_t;
	signal synth_cntrl				: synth_cntrl_vec_t(0 to 1);
	signal char_lcd_instr_wr		: std_logic;
	signal char_lcd_instr			: std_logic_vector(3 downto 0);
	signal char_lcd_instr_data		: std_logic_vector(15 downto 0);
	signal char_lcd_instr_result	: std_logic_vector(15 downto 0);
	signal char_lcd_busy			: std_logic;
	signal game_state				: snake_game_state_t;


	signal lcd_data			: std_logic_vector(7 downto 0);
	signal lcd_en			: std_logic;
	signal lcd_rw			: std_logic;
	signal lcd_rs			: std_logic;


	constant CLK_FREQ : integer := 25_000_000;
	constant TIMING_SCALAR : integer := integer(ceil(real(270_000/25_000_000)));
	constant CLK_PERIOD : time := 40 ns; -- 1 000 000 000 / 25 000 000
	signal stop_clock : boolean := false;

	constant c : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(32 + 1526618 mod 94,8));
begin
	uut : snake_game
		port map(
			clk						=>	clk,
			res_n					=>	res_n,
			instr_wr				=>	instr_wr,
			instr					=>	instr,
			instr_data				=>	instr_data,
			instr_result			=>	instr_result,
			busy					=>	busy,
			vblank					=>	vblank,
			cntrl1					=>	cntrl1,
			cntrl2					=>	cntrl2,
			synth_cntrl				=>	synth_cntrl,
			char_lcd_instr_wr		=>	char_lcd_instr_wr,
			char_lcd_instr			=>	char_lcd_instr,
			char_lcd_instr_data		=>	char_lcd_instr_data,
			char_lcd_instr_result	=>	char_lcd_instr_result,
			char_lcd_busy			=>	char_lcd_busy,
			game_state				=>	game_state
		);

	char_lcd_cntrl_inst : char_lcd_cntrl
		port map(
			clk				=> clk,
			res_n			=> res_n,
			instr_wr		=> char_lcd_instr_wr,
			instr			=> char_lcd_instr,
			instr_data		=> char_lcd_instr_data,
			instr_result	=> char_lcd_instr_result,
			busy			=> char_lcd_busy,
			lcd_data		=> lcd_data,
			lcd_en			=> lcd_en,
			lcd_rw			=> lcd_rw,
			lcd_rs			=> lcd_rs
		);


	stimulus : process
		variable i : integer := 123;
	begin

		report "-----START------";
		-- initialisation

		-- char_lcd_busy <= '0';
		res_n		<= '0';
		busy 		<= '0';
		vblank		<= '1';
		-- char_lcd_busy <= '1';
		instr_wr	<= '0';
		instr		<= (others => '0');
		instr_data	<= (others => '0');
		instr_result <= (others => '0');
		wait for 5*CLK_PERIOD;
		res_n 		<= '1';
		cntrl1 <= N64_BUTTONS_RESET_VALUE;
		cntrl2 <= N64_BUTTONS_RESET_VALUE;

		wait for 40 us;
		cntrl1.btn_a <= '1';
		cntrl2.btn_a <= '1';
		wait for 5*CLK_PERIOD;
		cntrl1.btn_a <= '0';
		cntrl2.btn_a <= '0';

		wait for 1 ms;
		cntrl1.btn_z <= '1';
		wait for 5*CLK_PERIOD;
		cntrl1.btn_z <= '0';
		-- char_lcd_busy <= '0';

		wait for 50 us;
		instr_data <= COLOR_BLUE & COLOR_BLACK & "00000000";

		wait for 1000 ms;


		report to_string(to_unsigned(5,5));
		report "-----END-----";
		stop_clock <= true;
		wait;
	end process;


	generate_clk : process
	begin
		while not stop_clock loop
		clk <= '0', '1' after CLK_PERIOD / 2;
		wait for CLK_PERIOD;
	end loop;
	wait;
	end process;
end architecture;
