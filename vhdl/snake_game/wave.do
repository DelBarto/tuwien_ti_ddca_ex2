onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider generic
add wave -noupdate /snake_game_tb/clk
add wave -noupdate /snake_game_tb/res_n


add wave -noupdate -divider I/O
add wave -noupdate /snake_game_tb/char_lcd_instr_wr
add wave -noupdate /snake_game_tb/char_lcd_instr
add wave -noupdate /snake_game_tb/char_lcd_instr_data
add wave -noupdate /snake_game_tb/char_lcd_instr_result
add wave -noupdate /snake_game_tb/char_lcd_busy
add wave -noupdate /snake_game_tb/cntrl1.btn_a
add wave -noupdate /snake_game_tb/cntrl1.btn_z
add wave -noupdate -divider uut
add wave -noupdate /snake_game_tb/uut/state
add wave -noupdate /snake_game_tb/uut/game_state
add wave -noupdate /snake_game_tb/uut/score
add wave -noupdate /snake_game_tb/uut/highscore
add wave -noupdate /snake_game_tb/uut/sf_cntdwn

add wave -noupdate -divider lcd_writer
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/clk
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/res_n
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/state
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/buf_text
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/buf_sg_score
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/buf_sg_highscore
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/buf_sg_game_state
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/char_lcd_instr_wr
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/char_lcd_instr
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/char_lcd_instr_data
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/char_lcd_instr_result
add wave -noupdate /snake_game_tb/uut/lcd_writer_inst/char_lcd_busy

add wave -noupdate -divider char_lcd_cntrl
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/busy
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/lcd_controller_inst/state

add wave -noupdate -divider fsm
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/wr
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/lcd_cntrl_state
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/return_state
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/x_cursor
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/y_cursor
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/cntrl_en
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/cntrl_rw
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/cntrl_rs
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/cntrl_busy
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/cntrl_data_in
add wave -noupdate /snake_game_tb/char_lcd_cntrl_inst/controller_inst/cntrl_data_out

add wave -noupdate -divider food_placer
add wave -noupdate /snake_game_tb/uut/food_placer_inst/state
add wave -noupdate /snake_game_tb/uut/food_placer_inst/busy
add wave -noupdate /snake_game_tb/uut/food_placer_inst/done
add wave -noupdate /snake_game_tb/uut/food_placer_inst/wr
add wave -noupdate /snake_game_tb/uut/food_placer_inst/sd_food
add wave -noupdate /snake_game_tb/uut/food_placer_inst/buf_sd_food
add wave -noupdate /snake_game_tb/uut/food_placer_inst/food_type
add wave -noupdate /snake_game_tb/uut/food_placer_inst/tm_busy
add wave -noupdate /snake_game_tb/uut/food_placer_inst/tm_instr_wr
add wave -noupdate /snake_game_tb/uut/food_placer_inst/rnd_vec
add wave -noupdate /snake_game_tb/uut/food_placer_inst/rand
add wave -noupdate /snake_game_tb/uut/food_placer_inst/rand_pos
add wave -noupdate /snake_game_tb/uut/food_placer_inst/sf
