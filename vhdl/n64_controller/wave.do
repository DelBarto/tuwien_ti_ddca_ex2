onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider colors
add wave -noupdate /n64_controller_tb/clk
add wave -noupdate /n64_controller_tb/res_n
add wave -noupdate /n64_controller_tb/data
add wave -noupdate -divider colors
add wave -noupdate /n64_controller_tb/button_state
add wave -noupdate -divider colors
add wave -noupdate /n64_controller_tb/uut/state
add wave -noupdate /n64_controller_tb/uut/shiftreg_btns
