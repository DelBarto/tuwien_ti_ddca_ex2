-- TASK 6: put your code here

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.n64_controller_pkg.all;

entity n64_controller_tb is
end entity;

architecture bench of n64_controller_tb is
	component n64_controller is
		generic (
			SYNC_STAGES : integer := 2;
			CLK_FREQ : integer := 50_000_000;
			REFRESH_TIMEOUT : integer := 50_000_000/1_000*8
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;

			data : inout std_logic;

			-- button outputs
			button_state : out n64_buttons_t
		);
	end component;

	signal clk : std_logic;
	signal res_n : std_logic;
	signal data : std_logic;
	signal button_state : n64_buttons_t;

	constant CLK_PERIOD : time := 20 ns; -- 1_000_000_000 / 50_000_000
	signal stop_clock : boolean := false;
begin
	uut : n64_controller
	port map(
		clk => clk,
		res_n => res_n,
		data => data,
		button_state => button_state
	);

	stimulus : process
		variable start_seq 		: std_logic_vector(8 downto 0) := "0000000ZZ";
		variable data_seq 		: std_logic_vector(8 downto 0) := "ZZZZZZZZZ";
		variable btn_values		: std_logic_vector(15 downto 0);
		constant matr_value		: std_logic_vector(13 downto 0)
						:= std_logic_vector(to_unsigned(1526618 mod 2**14, 14));
		variable i : integer 	:= 0;
		variable ax : integer 	:=1 ;
		variable axis_value 	: std_logic_vector(7 downto 0);
	begin
		report "-----START------";
		-- initialisation
		btn_values(15 downto 10) := matr_value(13 downto 8);
		btn_values(9 downto 8) := (others => '0');
		btn_values(7 downto 0) := matr_value(7 downto 0);
		report "btn_values: " & to_string(btn_values);

		res_n <= '0';
		data <= 'Z';
		wait for 1.5*CLK_PERIOD;
		res_n <= '1';

		while not (data_seq = start_seq) loop
			-- report "data_seq: " & to_string(data_seq);
			wait until data'event and data = '0';
			wait for 1.5 us;
			data_seq := data_seq(7 downto 0) & data;
		end loop;


		--  wirte values of button generated from matriclulation number
		wait for 2.5 us;
		for i in 15 downto 0 loop
			data <= '0';
			wait for 1 us;
			if (btn_values(i) = '1') then
				data <= '1';
			else
				data <= '0';
			end if;
			wait for 2 us;
			data <= '1';
			wait for 1 us;
		end loop;

		-- write x-axis to the value 1
		axis_value := std_logic_vector(to_signed(1, axis_value'length));
		report "x-axis_value: " & to_string(axis_value);
		for i in 7 downto 0 loop
			data <= '0';
			wait for 1 us;
			if axis_value(i) = '1' then
				data <= '1';
			else
				data <= '0';
			end if;
			wait for 2 us;
			data <= '1';
			wait for 1 us;
		end loop;

		-- write the y-axis to the value -1
		axis_value := std_logic_vector(to_signed(-1, axis_value'length));
		report "y-axis_value: " & to_string(axis_value);
		for i in 7 downto 0 loop
			data <= '0';
			wait for 1 us;
			if axis_value(i) = '1' then
				data <= '1';
			else
				data <= '0';
			end if;
			wait for 2 us;
			data <= '1';
			wait for 1 us;
		end loop;

		data <= 'Z';

		report "-----END-----";
		stop_clock <= true;
		wait;
	end process;



	generate_clk : process
	begin
		while not stop_clock loop
		clk <= '0', '1' after CLK_PERIOD / 2;
		wait for CLK_PERIOD;
	end loop;
	wait;
	end process;
end architecture;
